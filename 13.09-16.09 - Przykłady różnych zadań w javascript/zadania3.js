function MyObject (msg)	//1
{
	if (msg)	//2
	{
		this.sentence = msg;	//3
	}
}
MyObject.prototype.sentence = 'Witaj świecie';	//4
MyObject.prototype.sayHi = function()
{
	alert(this.sentence);	//5
}
var oMyObj_one = new MyObject(); //6
var oMyObj_two = new MyObject("A ja nie będę taki jak inni"); //7
var oMyObj_three = new MyObject(); //6
oMyObj_three.sentence = 'Za to ja to już w ogóle jest krejzol'; //8
oMyObj_one.sayHi();	//9
oMyObj_two.sayHi();	//9
oMyObj_three.sayHi();	//9



//Stworzymy obiekt, ktory bedzie realizowal liczenie pola kwadratu lub
//prostokata w zaleznosci od przekazanych parametrow
//czy bedzie przekazany jeden czy dwa

var Pole = function(a,b){
    this.a = a;
    this.b = b;
    this.getPole = function(){
        if (a&&!b){
            return this.a *this.a;
        }
        else if (a&&b){
            return this.a * this.b;
        }
        else {
            console.log('Błąd.');
            return null;
        }
    }
    this.getObwod = function (){
    	if (a&&!b){
            return this.a *4;
        }
        else if (a&&b){
            return this.a *2 + this.b*2;
        }
        else {
            console.log('Błąd.');
            return null;
        }
    }
    this.przekatna = function(){
    	if (a&&!b){
            return Math.sqrt(this.a*this.a+this.a*this.a);
        }
        else if (a&&b){
            return Math.sqrt(this.a*this.a + this.b*this.b);
        }
        else {
            console.log('Błąd.');
            return null;
        }
    }
}
var oPoleKw = new Pole(5);
console.log('Pole kwadratu to: '+oPoleKw.getPole());
var oPoleProst = new Pole(5,10);
console.log('Pole prostokąta to: '+oPoleProst.getPole());
console.log('Pole prostokąta to: '+oPoleKw.getObwod());
console.log('Pole prostokąta to: '+oPoleProst.getObwod());

//
//Napisz obiekt RAND, ktory bedzie zawieral konstruktor z dwoma parametrami
//a i b (reprezentuajacymi zakres) oraz metody:
//        a) getNrandomNums(n) - zwracający tablice n losowych liczb z zakresu <a,b>
//        b) generateNumberFromRange() - zwracajaca losowa wartosc z podanego zakresu
//        c) generateSmallNum () - zwracajaca numer od 0 do 10
//        d) generateBigNum () - zwracajaca numer powyzej 100000 (maksymalnie 10 000 000)
//Jezeli do konstruktora zostanie przekazana liczba b mniejsza od a, konstruktor powinien
//je zamienic miejscami. Wskazówka - mozna napisac metode generujaca zakres i wywolywac
//ja z odpowiednimi parametrami w przypadku innych metod.
//Pokaz dzialanie poszczegolnych metod na stworzonej instancji obiektu.


var RAND = function(a,b){
    if(b<a){
    	this.a = b;
    	this.b = a;
    }
    else{
   	this.a = a;
    this.b = b;
    }
    this.generateNumberFromRange = function(a,b){
    	a = a || this.a; //bo jak nie przekazemy do metody argumentow to wezmie te z tworzenia obiektu
    	b = b || this.b;
    	return a+Math.round(Math.random()*(b-a));
        
    }
    this.generateSmallNum = function(){
    	
    	return this.generateNumberFromRange(1,11)-1;
    }
    this.generateBigNum = function (){
    	return this.generateNumberFromRange(100000, 10000000);
    	
    }
    this.getNrandomNums = function(n){
    	var tablica = [];
    	for (var i=0; i<n; i++){
    	tablica.push(this.generateNumberFromRange());
    	}
    	return tablica;
    }
    
}
var MyObj = new RAND(20,30);
console.log(MyObj.generateNumberFromRange());
console.log(MyObj.generateSmallNum());
console.log(MyObj.generateBigNum());
console.log(MyObj.getNrandomNums(10));

//1. Napisz funkcje, ktora wylosuje liczbe od 1 do 6
//2.napisz funkcje ktora wyemuluje 100 rzutow kostka
//3. napisz funkcje zliczajaca ile razy wypdal dany numer

var DICE = function(n){
	this.n=n;
	this.rollTable = [];
	this.counts = {};
	this.diceRoll = function(){
		return Math.round(Math.random()*5)+1;
	}
	this.generateRollTable = function (){
		for (var i=0; i<this.n; i++){
			this.rollTable.push(this.diceRoll());
		}
	}
	this.countVars = function (){
		
		for (var i=0; i< this.rollTable.length; i++){
			var num = this.rollTable[i];
			this.counts[num] = this.counts[num] ? this.counts[num]+1 : 1;
		}
	}
	this.init = function (){
		this.generateRollTable();
		this.countVars();
		console.log(this.counts);
	}
	this.init();
}
var oDice = new DICE(100);


//wieza babel nieskończone

function babel(height) {
  if (height == 0){
      return '';
  }
  else{
  var tab=[];
  function reverse(s){
    return s.split("").reverse().join("");
}
  for (var i=0; i<height; i++){
  	for (var j=0; j<3; j++){
  		tab.push('*')
	  console.log(' '.repeat(height-i)+tab[j]);
  	}
  
  
  }
}
}

babel(5);
//pominięte na zajęciach - do zrobienia
//Stwórz obiekt Punkt, który będzie odzwierciedleniem punktu na płaszczyźnie.
//        Punkt powinien mieć pola reprezentujące jego pozycję tj. x, y oraz
//metody pozwalające na manipulację punktem tj. zwiekszX - zwieksza x o 1, zwiekszY
//        - analogicznie, zmienX - zmienia wartosc na podana, zmienY - analogicznie.
//        Pokaż działanie obiektu na nowo zainicjalizowanej instancji.