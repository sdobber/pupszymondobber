
//substring (poczatek, koniec)
var str='programowanie';
console.log(str.substring(10));
console.log(str.substring(3,7));
console.log(str.substring(6,10));
console.log((str.substring(1,4))+(str.substring(11,12)));
console.log((str.substring(4,7))+(str.substring(9,10)));

////1. Dane jest osiedle bloków. Każdy blok zawiera 2 klatki, po 2 piętra (+ parter), na każdym piętrze
//znajdują się dwa mieszkania. Napisz skrypt, który wyświetli adres każdego mieszkania tak, aby można
//było zaadresować kopertę. Bloki znajdują się tylko pod numerami nieparzystymi - pierwszy blok
//znajduje się pod numerem 1. Funkcja powinna przyjmować nazwę ulicy oraz ostatni numer, który
//występuje na danej ulicy (może być parzysty lub nieparzysty) oraz na wyjściu drukować poszczególne
//adresy.

function adres (ulica,numer){
	numer = parseInt(numer);
	var klatka;
	
	for (var i=1; i<=numer; i++){
		if (i%2==1){
			klatka = 'A';
			for (var j=1;j<=6;j++){
			console.log(ulica+' '+i+klatka+'/'+j);
			}
			klatka = 'B';
			for (j=1;j<=6;j++){
			console.log(ulica+' '+i+klatka+'/'+j);
			}
		}
	}
}
var x = prompt('Podaj nazwę ulicy.');
var y = prompt('Podaj ostatni numer ulicy.');
adres(x,y);


//2. Napisz funkcję, sprawdzającą czy przekazany argument jest palindromem w zależności od
//przekazanego typu danych. Dla przykładu, funkcja zwraca True dla:
//- 'oko'
//- 'kajak'
//- 5225
//- 'no i lata batalion', 'kobyla ma maly bok' (należy uwzględnić pomijanie spacji przy sprawd zaniu
//palindromu)
//- [1, 2, 3, 2, 1]

function palindrom (tab){
	var tablica=[];
	var wartosc;
	for (var i=0; i<tab.length; i++){
		if (tab[i]!=' ' && tab[i]!=',' &&tab[i]!='[' && tab[i]!=']'){
			tablica.push(tab[i]); //musi byc push do drugiej tablicy, bo jak nie
                                              //to wtedy pierwszy index jest rowny 0 a nastepny 2
                                              //i wtedy wartosc w indexie 1 jest undefined
			
		}
	}
	for (i=0; i<tablica.length; i++){
		
		if (tablica[i]==tablica[tablica.length-(i+1)]){
			wartosc=true;
		}
		else{
			wartosc=false;
		}
		
	}
	return wartosc;
}
var x = prompt('Podaj słowo.');
var y = x.split('');
palindrom(y);

//palindrom wersja z zajęć

function isPalindrome (arg){
    if (typeof arg == 'object'){
        arg = arg.join('');
    }
    arg = arg.toString();
    arg = arg.split(' ').join('');
    console.log(arg);
    for (var i=0; i< Math.floor(arg.length/2); i++){
        if (arg[i]!=arg[Math.floor(arg.length-i-1)]){
            return false;
        }
        else {
            return true;
        }
    }
}
var x = prompt('Podaj słowo.');
isPalindrome();


//3. Napisz funkcję sprawdzającą, czy podany numer pesel jest prawidłowy. Funkcja powinna zwracać
//datę urodzenia oraz płeć osoby.
//https://pl.wikipedia.org/wiki/PESEL#Cyfra_kontrolna_i_sprawdzanie_poprawno.C5.9Bci_numeru

function pesel (numer){
	var tablica=[];
	var miesiac;
	var plec;
	for (var i=0; i<numer.length; i++){
		
			tablica.push(numer[i]);
			
		
	}
	if (tablica.length<11){
		return 'Błędny numer pesel'
	}
	miesiac=parseInt(tablica[2]+tablica[3]);
	switch (miesiac){
		case 1:
			miesiac='Styczeń';
			break;
		case 2:
			miesiac='Luty';
			break;
		case 3:
			miesiac='Marzec';
			break;
		case 4:
			miesiac='Kwiecień';
			break;
		case 5:
			miesiac='Maj';
			break;
		case 6:
			miesiac='Czerwiec';
			break;
		case 7:
			miesiac='Lipiec';
			break;
		case 8:
			miesiac='Sierpień';
			break;
		case 9:
			miesiac='Wrzesień';
			break;
		case 10:
			miesiac='Październik';
			break;
		case 11:
			miesiac='Listopad';
			break;
		case 12:
			miesiac='Grudzień';
			break;
		default:
		return 'Podałeś nieprawidłowy numer miesiąca.';
	}
	if (tablica[9]%2==0){
		plec = 'kobieta';
	}
	else {
		plec = 'mężczyzna';
	}
	var sprawdzenie = 1*tablica[0]+3*tablica[1]+7*tablica[2]+9*tablica[3]+1*tablica[4]+3*tablica[5]+7*tablica[6]+9*tablica[7]+1*tablica[8]+3*tablica[9];
	sprawdzenie=sprawdzenie%10;
	if (sprawdzenie==0){
		sprawdzenie=0;
	}
	else{
		sprawdzenie=10-sprawdzenie;
	}
	if (tablica[10]==sprawdzenie){
		return 'Data urodzenia: '+tablica[4]+tablica[5]+' '+miesiac+' 19'+tablica[0]+tablica[1]+' '+plec;
		
		
	}
	else {
	return 'Błędny numer pesel';
	}
}
var x = prompt('Podaj numer pesel.');
var y = x.split('');
pesel(y);

//pesel wersja z zajęć
function zeros (arg, num){
	arg = arg.toString();
	var temp = '';
	if (arg.length<num){
		temp = "0".repeat(num - arg.length);
	}
	temp = temp+arg;
	return temp;
}
	
function checkPESEL(pesel){
	var day = parseInt(pesel.substring(4,6)),
		month = parseInt(pesel.substring(2,4)),
		year = parseInt('19'+pesel.substring(0,2));
		sex = (parseInt(pesel[9])%2 == 0)? 'Kobieta':'Mężczyzna';
		if (month>20){
			month = month-20;
			year = year+100;
		}
		var sumaWag = 0;
		var tablica1 = [1,3,7,9,1,3,7,9,1,3,1];
		var tablica2 = pesel.split('');
		for (i=0; i<tablica1.length;i++){
			sumaWag = sumaWag+tablica1[i]*parseInt(tablica2[i]);	
		}
		if(sumaWag % 10 != 0){
			return false;
		}
		console.log(sumaWag);
		console.log(zeros(day,2)+'.'+zeros(month,2)+'.'+year+' |płeć: '+sex);
		return true;
}
checkPESEL('');


//4. Napisz klasę emulującą działanie szyfru Cezara. Klasa powinna zawierać metodę szyfrującą oraz
//deszyfrującą (oraz ewentualnie metodę). Przyjmij, że szyfr powinien działać dla małych liter, bez
//polskich znaków.
//https://pl.wikipedia.org/wiki/Szyfr_Cezara
//wersja 2.0 dodano liczbę ustawiającą przesunięcie

function szyfrCezara (slowo,szyfr){
	this.number=slowo;
	this.cipher_number=parseInt(szyfr);
	this.cipher = function(){
		var zmienna=[];
		for (var i=0; i<this.number.length; i++){
			if (this.number[i].charCodeAt(0)>=97 && this.number[i].charCodeAt(0)<=122){
			if (this.number[i].charCodeAt(0)==32){
				zmienna[i]=' ';
			}
			else{
			zmienna[i] = String.fromCharCode((this.number[i].charCodeAt(0))+this.cipher_number);
			if(zmienna[i].charCodeAt(0)>122){
				zmienna[i]=String.fromCharCode(zmienna[i].charCodeAt(0)-26);
			}
			}
			}
			else {
				return 'Należy wprowadzać tylko małe litery bez polskich znaków.';
			}
		}
		return zmienna.join('');
	}
	this.decipher = function(){
		var zmienna=[];
		for (var i=0; i<this.number.length; i++){
			if (this.number[i].charCodeAt(0)>=97 && this.number[i].charCodeAt(0)<=122){
			if (this.number[i].charCodeAt(0)==32){
				zmienna[i]=' ';
			}
			else {
			zmienna[i] = String.fromCharCode((this.number[i].charCodeAt(0))-this.cipher_number);
			if(zmienna[i].charCodeAt(0)<97){
				zmienna[i]=String.fromCharCode(zmienna[i].charCodeAt(0)+26);
			}
			}
			}
			else{
				return 'Należy wprowadzać tylko małe litery bez polskich znaków.';
			}
		}
		return zmienna.join('');
	}
}
var x = prompt('Podaj słowo.');
var y = x.split('');
var przesuniecie = prompt('Podaj liczbę szyfrującą.')
var sprawdzenie = new szyfrCezara(y,przesuniecie);
console.log(sprawdzenie.cipher());
console.log(sprawdzenie.decipher());

//szyfr cezara wersja na zajęciach

function caesarCipher (arg, num){
    var alphabet ='',
        cipher = [],
        cipheredArg = '';
        for (var i=97; i<=122; i++){
            alphabet += String.fromCharCode(i);
        }
        alphabet = alphabet.split('');
        cipher = alphabet.slice(num).concat(alphabet.slice(0,num));
        for (var i=0; i< arg.length; i++){
            if (alphabet.indexOf(arg[i])>=0){
            cipheredArg +=cipher[alphabet.indexOf(arg[i])];
            
        }
        else{
            cipheredArg += arg[i];
        }
        }
        return cipheredArg;
    }
    caesarCipher('przyklad', 7); //jak będzie '-7' to mamy decipher


//5. Napisz funkcję emulującą działanie szyfrowania ROT13. Przyjmij założenie, że szyfrowanie nie
//występuje dla polskich znaków i występuje tylko i wyłącznie dla małych liter.
//https://pl.wikipedia.org/wiki/ROT13


function szyfrROT (numer){
	this.number=numer;
	
	this.cipher = function(){
		var zmienna=[];
		for (var i=0; i<this.number.length; i++){
			if (this.number[i].charCodeAt(0)>=97 && this.number[i].charCodeAt(0)<=122){
			if (this.number[i].charCodeAt(0)==32){
				zmienna[i]=' ';
			}
			else{
			zmienna[i] = String.fromCharCode((this.number[i].charCodeAt(0))+13);
			if(zmienna[i].charCodeAt(0)>122){
				zmienna[i]=String.fromCharCode(zmienna[i].charCodeAt(0)-26);
			}
			}
			}
			else {
				return 'Należy wprowadzać tylko małe litery bez polskich znaków.';
			}
		}
		return zmienna.join('');
	}
	this.decipher = function(){
		var zmienna=[];
		for (var i=0; i<this.number.length; i++){
			if (this.number[i].charCodeAt(0)>=97 && this.number[i].charCodeAt(0)<=122){
			if (this.number[i].charCodeAt(0)==32){
				zmienna[i]=' ';
			}
			else {
			zmienna[i] = String.fromCharCode((this.number[i].charCodeAt(0))+13);
			if(zmienna[i].charCodeAt(0)>122){
				zmienna[i]=String.fromCharCode(zmienna[i].charCodeAt(0)-26);
			}
			}
			}
			else{
				return 'Należy wprowadzać tylko małe litery bez polskich znaków.';
			}
		}
		return zmienna.join('');
	}
}
var x = prompt('Podaj słowo.');
var y = x.split('');
var sprawdzenie = new szyfrROT(y);
console.log(sprawdzenie.cipher());
console.log(sprawdzenie.decipher());


// ROT13 wersja na zajęciach

function ROT13(arg){
	var alphabet = '',
		cipher = '',
		cipheredWord = '';
		for (var i= 97; i<=122; i++){
			alphabet +=String.fromCharCode(i);
		}
		cipher = alphabet.substring(13) +alphabet.substring(0,13);
		for (var i=0; i<arg.length; i++){
			cipheredWord += cipher[alphabet.indexOf(arg[i])];
		}
		return cipheredWord;
}
ROT13('hello');


//suma elementów zagnieżdżonej tablicy
function sumArrElems (arr){
	var sum = 0;
	for (var i in arr){
		if (typeof arr[i] =='number'){
			sum = sum + arr[i];
		}
		else if (typeof arr[i] == 'object'){
			sum = sum + sumArrElems(arr[i]);
		}
	}
	return sum;
}

sumArrElems([1, 2, 4, [5, 6]]);



//dzielenie tablic
var x = [5,6,7,13,45];

var y = x.slice(1,3);
var z = x.slice(3,4);
var a = x.slice(0,3);
console.log(y, z, a);


//6. Napisz skrypt który dokona obliczeń, dla wprowadzonego ciągu znaków w odwrotnej notacji
//polskiej.
//Informacje nt Odwrotnej notacji polskiej: https://pl.wikipedia.org/wiki/Odwrotna_notacja_polska
//a) do przetrzymywania danych użyj listy
//b) do przetrzymywania danych użyj klasy stosu

function list (val){ //nieskończone
	
	this.value=val;
	var odjeta_wartosc;
	this.dzialanie = function(a){
		var b,c, wynik;
		this.value[this.value.length] = a;
		if (a=='+'){
			sprawdzenie.odejmij();
			sprawdzenie.odejmij();
			b=parseInt(odjeta_wartosc);
			sprawdzenie.odejmij();
			c=parseInt(odjeta_wartosc);
			wynik=b+c;
			this.value[this.value.length]=wynik;
			return 'Wykonano działanie.';
			
		}
		if (a=='-'){
			sprawdzenie.odejmij();
			sprawdzenie.odejmij();
			b=parseInt(odjeta_wartosc);
			sprawdzenie.odejmij();
			c=parseInt(odjeta_wartosc);
			wynik=b-c;
			this.value[this.value.length]=wynik;
			return 'Wykonano działanie.';
			
		}
		if (a=='*'){
			sprawdzenie.odejmij();
			sprawdzenie.odejmij();
			b=parseInt(odjeta_wartosc);
			sprawdzenie.odejmij();
			c=parseInt(odjeta_wartosc);
			wynik=b*c;
			this.value[this.value.length]=wynik;
			return 'Wykonano działanie.';
			
		}
		if (a=='/'){
			sprawdzenie.odejmij();
			sprawdzenie.odejmij();
			b=parseInt(odjeta_wartosc);
			sprawdzenie.odejmij();
			c=parseInt(odjeta_wartosc);
			wynik=b/c;
			this.value[this.value.length]=wynik;
			return 'Wykonano działanie.';
			
		}
		
		return 'Dodano '+a;
	}
	this.odejmij = function(){
		var tab_pomoc=[];
		odjeta_wartosc = this.value[(this.value.length)-1];
		for (var i = 0; i<(this.value.length)-1; i++){
		tab_pomoc[i]=this.value[i];
		}
		this.value=[];
		for (i=0; i<tab_pomoc.length; i++){
			this.value[i]=tab_pomoc[i];
		}
		return 'Odjęto wartość.'
	}
	this.pokaz = function(){
		for(var i=0; i<this.value.length; i++){
			console.log(this.value[i]);
		}
		return 'Pokazano listę.'
	}
	
}
var x = prompt('Podaj ciąg znaków w odwrotnej notacji polskiej.');
var y = x.split('');
var sprawdzenie = new list(y);


//7. Gra - zgadywanie liczby naturalnej. System losuje liczbę z zadanego przedziału, następnie
//użytkownik proszony jest o podanie kolejnych liczb. Jeżeli liczba jest taka sama jak wygenerowana
//należy wyświetlić odpowiedni komunikat oraz zakończyć działanie skryptu.
//Przyjmij, że na początku użytkownik:
//a) podaje liczbę maksymalnych prób zgadnięć (po przekroczeniu prób, wyświetlana jest wylosowana
//liczba oraz komunikat o niepowodzeniu)
//b) nie podaje maksymalnej liczby zgadnięć - skrypt działa dopóki użytkownik nie zgadnie liczby, przy
//wpisaniu -1 skrypt powinien wyświetlić ilość podjętych prób, wygenerowaną liczbę oraz zakończyć
//działanie

function dzialanie(liczba){
	if (liczba==undefined){
		var wylosowana_liczba = losowa();
		var podana_liczba;
		var counter=0;
		while (podana_liczba != wylosowana_liczba){
			
			podana_liczba = prompt('Podaj liczbę.');
			podana_liczba = parseInt(podana_liczba);
			if (podana_liczba == -1){
				return 'Zgadywałeś '+counter+' razy. Losowa liczba to: '+wylosowana_liczba;
			}
			counter++;
		}
		return 'Zgadłeś liczbę. Ta liczba to: '+wylosowana_liczba;
	}
	else{
		liczba=parseInt(liczba);
		wylosowana_liczba = losowa();
		for (var i=0; i<liczba; i++){
			podana_liczba = prompt('Podaj liczbę.');
			podana_liczba = parseInt(podana_liczba);
			if (podana_liczba == wylosowana_liczba){
				return 'Zgadłeś liczbę. Ta liczba to: '+wylosowana_liczba;
			}
		}
		return 'Przekroczono maksymalną liczbę zgadnięć.';
		
	}
}
function losowa (){
	var losowa_liczba = Math.floor(Math.random()*10);
	return losowa_liczba;
}
var x = prompt('Czy podajesz maksymalną liczbę zgadnięć?');
if (x=='Tak'){
	var y = prompt('Podaj maksymalną liczbę zgadnięć.');
	dzialanie(y);
}
else if (x=='Nie'){
	dzialanie();
}
else{
	alert('Podałeś nieprawidłową odpowiedź.');
}

//8. Gra - zgadywanie słów. Zadany jest słownik ze słowami. System losuje jedno słowo, następnie
//zamienia jego litery na _ oddzielone spacją. Użytkownik proszony jest o podanie liter, jeżeli dana
//litera występuje w słowie, zostaje odkryta (zamieniamy _ na daną literę na wszystkich pozycjach, w
//których występuje). Załóż, że gra kończy się wtedy, gdy użytkownik poda -1 na wejściu zamiast litery,
//albo gdy zostaną odgadnięte wszystkie litery - w takim przypadku wyświetl komunikat o powodzeniu.


var slownik = ['abecadło','czosnek','ananas','palindrom','oksymoron', 'indywidualizacja', 'lol','losowanie','niepodległość','matematyka'];

function losowa (){
	var losowa_liczba = Math.floor(Math.random()*10);
	return losowa_liczba;
}
var wylosowana_liczba = losowa();
var ukryte_slowo = slownik[wylosowana_liczba-1].split('');
var zaszyfrowane=[];
for (var i=0;i<ukryte_slowo.length;i++){
	zaszyfrowane[i]='_';
}
function zgadywanie(){
	alert(zaszyfrowane.join(' '));
	while (zaszyfrowane.indexOf('_')!=-1){
		x = prompt('Podaj literę');
	if (x==-1){
		return 'Zakończyłeś grę.';
	}
	for (i=0; i<ukryte_slowo.length; i++){
		if (x==ukryte_slowo[i]){
			zaszyfrowane[i]=x;
		}
	}
	alert(zaszyfrowane.join(' '));
	}
}

zgadywanie();

//dodatkowe
//Dla liczby 13`95, wsytępują dzielniki, które są liczbami pierwszymi: 5, 7, 13, 29.
//Jaki jest największy dzielnik, który jest liczbą pierwszą dla 600851475143?
//var liczba = 600851475143
//var liczba = 600851475143
var tablica=[];
function isPrime(n) {
   for (var i = 2; i < n; i++) {
       if (n % i == 0) {
           return false;
       }
   }
   return true;
}

function printPrimesFromRange(a, b) {
   for (var i = a; i <= b; i++) {
       if (isPrime(i)) {
           tablica.push(i);
       }
   }
}
printPrimesFromRange(2, 10000);


function faktoryzacja(tab){
	var suma=0;
	var liczba=parseInt(tab.join(''));
	var tab_primes=[];
	for (var i=0; i<tablica.length; i++){
		if(liczba%tablica[i]==0){
			tab_primes.push(tablica[i]);
			liczba/tablica[i];
		}
	}
	console.log(tab_primes[tab_primes.length-1]);
}
var x = prompt('Podaj liczbę.');
var y = x.split('');
faktoryzacja (y);