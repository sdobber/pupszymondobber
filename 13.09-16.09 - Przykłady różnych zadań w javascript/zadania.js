//1. Napisz funkcję zwracającą wartość bezwzględną liczby.

function myAbs(num){
	
	if (num < 0){
		return -num;
	}
	else {
		return num;
	}
	
}

var x=prompt('podaj liczbe');
myAbs(x);


//2. Napisz funkcję, która dla zadanego dnia wyświetli jego dzień tygodnia (np. 1 - niedziela).
//(należy zbadać czy liczba mieści się w zakresie)

function dzientygodnia(dzien){
	if (dzien >= 1 && dzien<=7){
	switch (dzien){
		case '1':
			return 'Poniedziałek';
			break;
		case '2':
			return 'Wtorek';
			break;
		case '3':
			return 'Środa';
			break;
		case '4':
			return 'Czwartek';
			break;
		case '5':
			return 'Piątek';
			break;
		case '6':
			return 'Sobota';
			break;
		case '7':
			return 'Niedziela';
			break;
	}
	}
	else {
		return 'Podałeś nieprawidłowy dzień tygodnia.';
	}
}
var x = prompt('Podaj numer dnia tygodnia.');
dzientygodnia(x);

//3. Napisz funkcję, która zwróci nazwę miesiąca dla przekazanej liczby (należy zbadać czy liczba
//mieści się w zakresie) przy użyciu tablicy

function miesiac(number){
        number=parseInt(number);
	var tab =['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień']; 
	if (number >= 1 && number<=12){
	return tab[number-1];
	}
	
	else {
		return 'Podałeś nieprawidłowy numer miesiąca.';
	}
}
var x = prompt('Podaj numer miesiąca.');
miesiac(x);

//4. Napisz funkcję licząca NWD wykorzystując algorytm Euklidesa.


function NWD(a,b){
	a=parseInt(a);
	b=parseInt(b);
	while (a!=b){
	if (a>b){
		a=a-b;
	}
	else {
		b=b-a;
	}
	}
	return a;
}
var x = prompt('Podaj dwie liczby oddzielając je przecinkiem.');
var y = x.split(',');

NWD(y[0], y[1]);

//euklides inaczej
function NWD (a,b){
    if (b!=0){
        return NWD(b, a%b);
    }
    return a;
}
NWD (50,25);


//5. Napisz funkcję, która przyjmuje jako argumenty: g - godziny, m - minuty, s - sekundy, a
//następnie zwraca podany czas w sekundach.

function czas(g,m,s){
	g=parseInt(g);
	m=parseInt(m);
	s=parseInt(s);
	return (g*3600)+(m*60)+s;
}
var x = prompt('Podaj 3 liczby oddzielając je przecinkami.');
var y = x.split(',');

czas(y[0], y[1], y[2]);

//6. Użytkownik podaje dwie liczby całkowite a, b. algorytm ma za zadanie wypisać wszystkie
//parzyste liczby w kolejności rosnącej, a następnie wszystkie liczby nieparzyste w kolejności
//malejącej z przedziału <a;b>. niech a, b –liczby całkowite z zakresu 0-255. Np. dla danych
//wejściowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3.

function zakres(a,b){
	a=parseInt(a);
	b=parseInt(b);
	if (a>=0 && a<=255 && b>=0 && b<=255){
            //var nums = [];
	for (var i=a; i<=b; i++){
		if (i%2==0){
			console.log (i+', ');//nums.push(i);
		}
	}
	for (i=b; i>=a; i--){
		if (i%2==1){
			console.log (i+', ');//nums.push(i);
		}
	}
        //return nums;
	}
	else{
		return 'Podałeś liczby spoza zakresu';
	}
}
var x = prompt('Podaj 2 liczby oddzielając je przecinkiem.');
var y = x.split(',');

zakres(y[0], y[1]);

//7. Napisz funkcję, która dla zadanej liczby zwróci sumę kwadratów poszczególnych liczb od 1 do
//zadanej liczby. Przyjmij i zbadaj czy użytkownik przekazał liczbę w przedziale <0, 10>

function sumakwadratow(a){
	a=parseInt(a);
	
	var suma=0;
	if (a>=1 && a<=10){
	for (var i=1; i<=a; i++){
		suma= suma+(i*i);
	}
	return suma;
	}
	else{
		return 'Podałeś liczby spoza zakresu';
	}
}
var x = prompt('Podaj liczbę.');


sumakwadratow(x);

//8. Napisz algorytm liczący ile potrzeba elementów (bloczków) dla piramidy o poziomie N (1
//poziom 1 bloczek, 2 poziom, dwa bloczki itd.)

function ilebloczkow(a){
	a=parseInt(a);
	if (a==1){
		return 1;
	}
	else{
		return a+ilebloczkow(a-1);
	}
}
var x = prompt('Podaj liczbe oznaczającą poziom piramidy');


ilebloczkow(x);


//9. Kasia ulokowała w banku pewna ilość złotych na okres jednego roku. Oprocentowanie roczne
//w tym banku wynosi 19,4%. Napisz algorytm, który będzie obliczał ilość pieniędzy na koncie
//po jednym roku dla dowolnej sumy pieniędzy. Zmodyfikuj program tak, aby obliczał kwotę
//dla wczytanej liczby lat. 

function oprocentowanie(a,n){
	a=parseInt(a);
	n=parseInt(n);
	
	for (var i=0; i<n; i++){
		a=a+(a*0.194);
	}
	return a;
}
var x = prompt('Podaj 2 liczby oznaczające kwotę i liczbę lat, rozdziel je przecinkiem.');
var y = x.split(',');

oprocentowanie(y[0],y[1]);

//10. Z Krakowa do Zakopanego jest 132 kilometry. Napisz algorytm, który będzie podawał czas w
//jaki należy przebyć tę drogę przy różnych prędkościach (zakładamy, że pojazd porusza się całą
//drogę prędkością jednostajną).

function czasprzejazdu(predkosc){
	predkosc=parseInt(predkosc);
	var droga = 132;
	var t=droga/predkosc;
	var godziny = Math.floor(t);
	var minuty = Math.floor((t-godziny)*60);
	var sekundy = Math.floor(((t-godziny)-(minuty/60))*3600);
	return 'Potrzeba '+godziny + 'h '+minuty+'m '+sekundy+ ' s czasu na pokonanie trasy';
}
var x = prompt('Podaj liczbę oznaczającą prędkość km/h');

czasprzejazdu(x);


//11. Dla zadanej tablicy elementów liczb całkowitych, napisz program, który pokaże różnicę
//największego oraz najmniejszego elementu, przykładowo, tablica zawiera elementy -5, 3, 2,
//1, 10, wynik powinien wynosić 15, gdyż -5 to najmniejszy element, a 10 to największy. 

function roznica(tablica){
	
	
	var min=parseInt(tablica[0]);
	var max=parseInt(tablica[0]);
	for (var i=0; i<tablica.length; i++){
		tablica[i]=parseInt(tablica[i]);
	}
	for (i=0; i<tablica.length; i++){
		
		if (tablica[i]<min){
			min=tablica[i];
		}
		if (tablica[i]>max){
			max=tablica[i];
		}
	}
	
	return 'Różnica min i max w tablicy to '+(max-min);
	
}

var x = prompt('Podaj liczby oddzielając je przecinkami.');
var y = x.split(',');
roznica (y);


//12. Na farmie występuje n królików. Nieparzyste z nich, mają niestety tylko jedno ucho, parzyste
//są w komplecie. Dla wczytanego n wypisz ile uszu znajduje się na farmie.

function liczbauszu(ilosckrolikow){
	
	ilosckrolikow=parseInt(ilosckrolikow);
	var suma=0;
	for (var i=1; i<=ilosckrolikow; i++){
		if(i%2!=0){
			suma=suma+1;
		}
		if (i%2==0){
			suma=suma+2;
		}
	}               //by zoptymalizować to trzeba dać wzór Math.floor(1.5*ilosckrolikow)
                        // i wtedy nie trzeba iterować po wszystkich królikach
	return suma;
}

var x = prompt('Podaj liczbę królików.');

liczbauszu(x);

//13. Mamy dwie tablice liczb całkowitych, należy:
//1. wyświetlić te liczby, które występują w obydwu tablicach
//2. wyświetlić liczby z obu tablic, które się nie powtarzają

function tablice1(){
	
	var tab1=[1,-3,4,6,13,-24,7];
	var tab2=[1,-4,7,6,1,2,-10];
	var zmienna;
	for (i=0; i<tab1.length;i++){
		zmienna = tab1[i];
	for (var j=0; j<tab1.length; j++){
	if (zmienna == tab2[j]){
		console.log(zmienna);
	}
	}
	}
	return 0;
}

function tablice2(){
	
	var tab1=[1,-3,4,6,13,-24,7];
	var tab2=[1,-4,7,6,3,2,-10];
	var zmienna;
	var wartosc;
	for (i=0; i<tab1.length;i++){
		zmienna = tab1[i];
	for (var j=0; j<tab1.length; j++){
	if (zmienna !== tab2[j]){
		wartosc = true;
	}
	else {
		wartosc = false;
	}
	}
	if (wartosc == true){
		console.log(zmienna);
	}
	}
	return 0;
}

tablice1();
tablice2();



//function numNotInArrays (a, b){
//	getDiffNums(a, b);
//	getDiffNums(b, a);
//}
//function numInArrays(arr1, arr2){
//	for (var i=0; i<arr1.length; i++){
//		if (inArray(arr2,arr1[i])){
//			console.log(arr1[i]);
//		}
//	}
//}
//
//
//function getDiffNums (arr1, arr2){
//	for (var i=0; i<arr1.length; i++){
//		if (!inArray(arr2,arr1[i])){
//			console.log(arr1[i]);
//		}
//	}
//}
//
//
//function inArray (haystack, needle){
//	for (var i=0; i<haystack.length; i++){
//		if (haystack[i]==needle){
//			return true;
//		}
//	}
//	return false;
//}
//var tab1=[1,-3,4,6,13,-24,7];
//var tab2=[1,-4,7,6,1,2,-10];
//numInArrays(tab1, tab2);
//numNotInArrays(tab1, tab2);




//14. Napisz funkcję, w której dla zadanego łańcucha znaków, wszystkie znaki - takie same jak
//pierwsza litera ciągu znaków zostaną zamienione na znak '_', wyjątkiem jednak jest pierwszy
//znak. Dla przykładu:
//Wejście: oksymoron
//Wyjście: oksym_r_n 


function ciagznakow(tablica){
	
	
	var litera = tablica[0];
	for (var i=1; i<tablica.length; i++){
		if (tablica[i]==litera){
			tablica[i]='_';
		}
	}
	
	return tablica.join('');
	
}

var x = prompt('Podaj słowo');
var y = x.split('');
ciagznakow (y);


//15. Napisz funkcję, która przyjmie dwa argumenty: liczbę znaków oraz zdanie. Funkcja powinna
//zwrócić listę ze słowami, które są dłuższe niż zadana liczba w pierwszym argumencie.
//Przykład:
//Wejście: 3, "kiedyś się wybiorę do lasu"
//Wyjście: ['kiedyś', 'wybiorę', 'lasu'] 



function zdanie(n,tablica){
	
	
	
	for (var i=0; i<tablica.length; i++){
	
		if (tablica[i].length<=n){
			tablica[i] ='';
		}
	}
	
	return tablica.join(' ');
	
}
var x;
var y;
var z;
for (var j=0; j<2; j++){
if (j==0){
	x = prompt('Podaj liczbę');
}
if (j==1){
	y = prompt('Podaj zdanie oddzielając słowa spacjami.');
	z = y.split(' ');
}
}
zdanie(x,z);



//16. Napisz funkcję, która przyjmuje dwie listy jako argumenty. Jeżeli na obu listach występuje ten
//sam element, funkcja powinna zwracać True, w przeciwnym wypadku False. 

function listy(tablica1,tablica2){
	
	
	
	var zmienna;
	var wartosc;
	for (i=0; i<tablica1.length;i++){
		zmienna = tablica1[i];
	for (var k=0; k<tablica1.length; k++){
	if (zmienna == tablica2[j]){
		wartosc = true;
	}
	}
	}
	if (wartosc == true){
		return true;
	}
	else{
		return false;
	}
	
}
var x;
var y;
var z;
var zz;
for (var j=0; j<2; j++){
if (j==0){
	x = prompt('Podaj kilka słów oddzielając je przecinkami.');
	y=x.split(',');
}
if (j==1){
	z = prompt('Podaj kilka słów oddzielając je przecinkami.');
	zz = z.split(',');
}
}
listy(y,zz);


//17. Napisz funkcję, która wczytuje od użytkownika liczbę (n). Dla podanej liczby wydrukuj
//piramidę gwiazdek rosnąco oraz malejąco, jak na przykładzie:
//(n = 5):
//*
//* *
//* * *
//* * * *
//* * * * *
//* * * *
//* * *
//* *
//* 


function gwiazdki(n){
	
	
	for(var i=0; i<=n;i++){
		console.log('*'.repeat(i));
	}
	for(i=n-1; i>0;i--){
		console.log('*'.repeat(i));
	}
	return 0;
	
}
var x = prompt ('Podaj liczbę');
gwiazdki(x);



//18. Napisz funkcję, która dla wczytanego od użytkownika słowa, wyświetla jego litery w
//kolejności odwrotnej. 


function odwroconeslowo(slowo){
	
	var tablica=[];
	for(var i=0; i<=slowo.length;i++){
		tablica[i]=slowo[slowo.length - i];
	}
	return tablica.join('');
	// inna wersja
        //function reverseWord(word){
        //  return word.split('').reverse().join('');
        //}
        //reverseWord('costamcostam');
}
var x = prompt ('Podaj słowo');
var y = x.split('');
odwroconeslowo(x);

//19. Napisz funkcję, która jako argument przyjmuje dodatnią liczbę całkowitą większą od zera. Dla
//podanego zakresu wydrukuj kolejne wartości pomijając te, które są podzielne przez 3 lub
//przez 4.


function podzielne34 (n){
	for (i=1; i<=n; i++){
		if (i%3!=0 && i%4!=0){
			console.log(i);
		}
	}
}

var x = prompt('Podaj liczbę.');
podzielne34(x);