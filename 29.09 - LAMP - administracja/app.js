// Naszym kolejnym zadaniem będzie napisanie gry przeglądarkowej do zgadywania haseł. System losuje 1 słowo z zadeklarowanych, a następnie wyświetla słowo zakodowane w sposób: _ _ _ _
// Po prawej stronie znajdywać będą się litery, wygenerowane skryptowo (tj. umieszone automatycznie), po wciśnięciu których nastąpi odsłona litery w słowie, jeśli taka istnieje. Dodatkowo, jeżeli użytkownik wciśnie na klawiaturze wybrany klawisz, litera też powinna się odsłonić. Jeżeli litera została wciśnięta, przycisk powinien zostać zablokowany tak, aby nie można było go ponownie użyć dla wybranego słowa.
// Powyżej listy liter do wciśnięcia znajdywać się powinna lista użytych już liter.
// Liczba prób zgadywania: 2 * długość_słowa,
// Dodatkowo użytkownik ma możliwość 3 razy zgadnięcia hasła w inpucie, który znajduje się nad listą zgadywanych wyrazów.
// UWAGA! Przyjmij, że w strukturze html znajduje się tylko i wyłącznie (w sekcji body) <div class="fortune"></div>, reszta elementów tworzona jest przez skrypt.


/*
$(document).ready(function() {
    
    $('<div/>', { id : 'wrapper',
                  class: 'container' 
                }).prependTo('body');
    $('<div/>', { class : 'leftSide'}).html('Lewa strona').prependTo('#wrapper');
    $('<div/>', { class : 'rightSide'}).html('Prawa strona').appendTo('#wrapper');
    
    $(window).on('keyup', function(e) {
		console.log(String.fromCharCode(e.keyCode));
	});
});
*/

$(document).ready(function(){
    
    /*tworzymy elementy*/
    $('<div/>', {class : 'leftSide'}).prependTo('.fortune');
    $('<div/>', {class : 'rightSide'}).appendTo('.fortune');
    $('<div/>', {class : 'clear'}).appendTo('.fortune');
    $('<div/>', {class : 'guessBox'}).prependTo('.rightSide');
    
    $('<div/>', {class : 'counter'}).html('Ilość prób: <span></span').appendTo('.rightSide');
    
    $('<div/>', {class : 'usedLettersBox'}).html('Użyte litery: <span class="usedLetters"></span>').appendTo('.rightSide');
    $('<div/>', {class : 'letterBox'}).appendTo('.rightSide');
    $('<input/>', {name : 'guess', id : 'guess', type : 'text'}).appendTo('.guessBox');
    $('<button/>', {id : 'guessBtn'}).html('Zgaduj').appendTo('.guessBox');
    $('<div/>', {class : 'guessCounter'}).html('Ilość prób \"Zgaduj\": <span></span>').appendTo('.guessBox');
    $('<h1/>',{class : 'title'}).html('Koło fortuny').prependTo('.leftSide');
    $('<div/>',{class : 'phrase'}).html('_______').appendTo('.leftSide');
    
    /*generujemy buttony*/
    
    for (var i=65; i<=90; i++){
        $('<button/>', {id : String.fromCharCode(i), class : 'guessLetter'}).html(String.fromCharCode(i)).appendTo('.letterBox');
    }
    
    // deklaracja słów
    var words = ['oksymoron', 'jablko', 'banan', 'wrona', 'fajrant', 'urlop', 'przerwa', 'wolne', 'javascript'];
    var currentWord = words[(Math.random()*words.length)|0].toUpperCase();
    var dashedWord = "_".repeat(currentWord.length),
    usedLetters = [];
    dashedWord = dashedWord.split('');
    // umieszczamy zakodowane słowo
    $('.phrase').html(dashedWord);
    //ustawiamy ilość prób
    $('.counter').find('span').html(currentWord.length*2);
    $('.guessCounter').find('span').html(Math.round(currentWord.length/2));
    //splitujemy currentWord do tablicy
    var tab = currentWord.split('');
    
    
    function guessLetterGame (e){
       
       var currentLetter;
       if (e.type=='keyup'){
           if (e.keyCode>=65 && e.keyCode<=90){
               currentLetter = String.fromCharCode(e.keyCode);
           }
       }
       else
       {
           currentLetter = $(this).attr('id');
       }
      
       var $counter = $('.counter').find('span');
       var currentCounter = parseInt($counter.html());
       var $usedLetters = $('.usedLetters');
       if(e.target.tagName == 'INPUT' || usedLetters.indexOf(currentLetter) != -1){
           return;
       }
       for( var i=0; i<tab.length; i++){
       if(currentLetter == tab[i]){
           
           dashedWord[i] = tab[i];
           $('#'+currentLetter).addClass('greenBorder');//zamiast '#'+currentLetter może być this
       }
       else
       {
           $('#'+currentLetter).addClass('redBorder');
       }
       }
      
       $('.phrase').html(dashedWord);
      
       $counter.html(currentCounter-1);
      
       if ($usedLetters.html().length==0){
           $usedLetters.html(currentLetter);
       }
       else
       {
                   $usedLetters.append(', ' + currentLetter); 
           
       }
       $('#'+currentLetter).attr('disabled', 'disabled');
       usedLetters.push(currentLetter);
       // sprawdzenie wygranej
       if(checkForWin(dashedWord.join(''))){
           return;
       }
       // sprawdzenie przegranej
       if (currentCounter == 1){
           
           $('.guessLetter').attr('disabled','disabled');
           $('.guessLetter').off('click');
           $('#guessBtn').attr('disabled','disabled');
           $(document).off('keyup');
           $('.phrase').html('Przegrana (' + currentWord + ')');
           return;
       }
      
    }
    function checkForWin (word){
        if (word==currentWord){
           $('.guessLetter').parent().find('.guessLetter').attr('disabled','disabled');
          
           $('.guessLetter').off('click');
           $('#guessBtn').attr('disabled','disabled');
           $(document).off('keyup');
           $('.phrase').html('Wygrana ('+currentWord+')').addClass('win');
           return true;
       }
       return false;
    }
    
    //nasluchujemy klikniecia i podmieniamy literki
    $('.guessLetter').on('click', guessLetterGame); //jako drugi argument dajemy funkcję
    
    $(document).on('keyup', guessLetterGame);
    $('#guessBtn').on('click', function(){
        checkForWin($('#guess').val().toUpperCase());
        var $guessCounter = $('.guessCounter').find('span');
        currentAttempt = parseInt($guessCounter.html());
        $guessCounter.html(currentAttempt-1);
        if ($('#guess').val().toUpperCase()==currentWord) {
          $('#guessBtn').attr('disabled','disabled');
           $('#guessBtn').off('click');
           $(document).off('keyup');
           $('.phrase').html('Wygrana (' + currentWord + ')');
           return;
        }
        else
        {
            if (currentAttempt == 1) {
          $('#guessBtn').attr('disabled','disabled');
           $('#guessBtn').off('click');
           $(document).off('keyup');
           $('.phrase').html('Przegrana (' + currentWord + ')');
           $('.phrase').removeClass('win');
           return;
        }
    }
    });
        
    
   
   
});