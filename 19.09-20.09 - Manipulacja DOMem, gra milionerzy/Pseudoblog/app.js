var menuItems = document.querySelectorAll('ul > li > a');

for (var i=0; i<menuItems.length; i++){
    menuItems[i].addEventListener('click', function(e){ //pod 'e' wchodzi MouseEvent
                                                        //bo 'click' dotyczy MouseEvent
        showSection(e.target.dataset.show); //target to to na co kliknęliśmy
        e.preventDefault();                 //dataset to słowo klucz po ktorym jest
                                            //utworzony przez nas atrybut show
    });                                     //a dalej zapobiegamy temu by link był linkiem
}
function showSection(section){
    var sections = document.querySelectorAll('section');
    for (var i=0; i<sections.length; i++){
        if(sections[i].id==section){
            sections[i].classList.add('dBlock');
        }
        else {
            sections[i].classList.remove('dBlock');
        }

    }
}
//pobieramy element buttona (moze byc po id) i przypisujemy go do zmiennej
var wartosc_buttona = document.getElementById('sendBtn');
//dla tego elementu dodajemy event listener
wartosc_buttona.addEventListener('click', function(e){
    //w funckji anonimowej ktora jest parametrem funkcji addeventlistener
    //pobieramy do zmiennej wszystkie inputy, ktore znajduja sie w sekcji 'contact'
var zapytanie = document.querySelectorAll('#contact > p > input, #contact > p > textarea');
var str = '?';
//iterujemy sie poprzez zapytanie (pobrane wczesniej elementy), a nastepnie sprawdzamy czy:
for (var i=0; i<zapytanie.length; i++){
    //jego value nie jest puste
    if (zapytanie[i].value != ""){
        //jeśli nie są pustymi wartościami, dodajemy ich klucz(.name) oraz wartość
        str += zapytanie[i].name + '='+ zapytanie[i].value +'&';
    }
}
//usuwamy ostatni znak ze stworzonego ciagu (by nie pojawiał się '&'
//albo '?' jeżeli jest pusty ciąg znaków
str = str.substring(0, str.length-1);
console.log(str);
});

//tworzymy w console.logu spis tresci na podstawie menu
var spis = document.querySelectorAll('ul > li > a');
var str2 = '';
for (var i=0; i<spis.length; i++){
    str2 += i+1 +'. '+spis[i].innerText+"\n";
}
console.log(str2)

var teksty = document.querySelectorAll('section > p');
var poczatekikoniec = '';
for (var i=0; i<teksty.length; i++){
    poczatekikoniec += teksty[i].innerText.substring(0,3)+teksty[i].innerText.substring(
            teksty[i].innerText.length-3);
}
console.log(poczatekikoniec);
