function Samochod (mar, mod, rocz){
    this.mark = mar;
    this.model = mod;
    this.year = rocz;
    
    this.describe = function (){
        var desc = 'Opis Auta : <br/>';
        desc+='Marka: ' + this.mark + '<br/>';
        desc+='Model: ' + this.model + '<br/>';
        desc+='Rocznik: ' + this.year + '<br/>';
        desc+='<br/><br/>';
        return desc;
    }
}

function Kursant (imie, nazwisko, skille, skille_do_wyuczenia){
    this.name = imie;
    this.surname = nazwisko;
    this.skills = skille;
    this.skills_to_learn = skille_do_wyuczenia;
    
    this.description = function (){
        var desc = 'Opis Kursanta: <br/>';
        desc += 'Imię: ' + this.name + '<br/>';
        desc += 'Nazwisko: ' +this.surname + '<br/>';
        return desc;
    }
    this.describe_skills = function (){
        var desc_skl = 'Opis umiejętności: <br/>';
        desc_skl += 'Obecne skille: ' + this.skills + '<br/>';
        desc_skl += 'Skille do wyuczenia: ' + this.skills_to_learn + '<br/>';
        return desc_skl;
    }
    this.change_skills = function (skills2, skills_to_learn2){
        this.skills = skills2;
        this.skills_to_learn = skills_to_learn2;
    }
}

//var Kursant1 = new Kursant('tom', 'kowalski', 'zero', 'wszystkie');
//console.log(Kursant1.describe_skills());
//Kursant1.change_skills('jeden','wszystkie bez jeden');
//console.log(Kursant1.describe_skills());

function Count (){
    this.suma = function(a,b){
        return a+b;
    }
    this.roznica = function(a,b){
        return a-b;
    }
    this.iloczyn = function (a,b){
        return a*b;
    }
    this.iloraz = function (a,b){
        return a/b;
    }
    this.potega = function (a,b){
        if (b==0){
        return 1;
    }
    var jak = false;
    var wynik=1;
    if (b<0) {
        jak = true;
        b*= -1;
    }
    while (b>0){
        wynik*=a;
        b--;
    }
    if (jak === true){
        return 1 / wynik;
    }
    else {
        return wynik;
    }
    }
    this.silnia = function (a){
        if (a==0){
        return 1;
    }
    else if (a==1){
        return 1;
    }
    else {
        return (a*this.silnia(a-1));
    }
}
    this.sumaCyfr = function (numb){
        var wynik = 0;
        numb = numb.toString();
        
        var singles = numb.split('');
        for (var i=0; i<singles.length; i++){
            wynik+= parseInt(singles[i]);
        }
        return 'Suam cyfr liczby ' +numb + ' to '+ wynik;
    }
    this.oblicz = function (a,b, type){
        a=parseInt(a);
    b=parseInt(b);
    
    if (isNaN(a) || (isNaN(b) && type!='silnia' && type!='cyfry')){
        
        return 'Nieprawidłowe wartości';
    }
    if (!type){//bo nie prawda
        return 'Nie podałeś typu działania.';
    }
    switch (type){
        case '+':
            return this.suma(a,b); //nie trzeba używać break'a do kończenia poszczególnego switch, bo mamy już
                                //return ktore konczy działanie
        case '-':
            return this.roznica(a,b);
        case '*':
            return this.iloczyn(a,b);
        case '/':
            return this.iloraz(a,b);
        case '^':
            return this.potega(a,b);
        case 'silnia':
            return this.silnia(a);
        case 'cyfry':
            return this.sumaCyfr(a);
        default: 'Podałeś nieprawidłowy typ działania.';
    }
};
}

window.onload = function(){
    var obliczanie = new Count();
    console.log(obliczanie);
var button = document.getElementById('button');
button.onclick = function (){
    var l1 = document.getElementById('l1').value;
    var l2 = document.getElementById('l2').value;
    var type = document.getElementById('type').value;
    var result = obliczanie.oblicz(l1, l2, type);
    document.getElementById('results').innerText = result;
};
document.getElementById('type').onkeyup = function(){
    if (this.value =='silnia'){
        document.getElementById('znikaj').style.display = 'none';
    }
    else
    {
        document.getElementById('znikaj').style.display = 'block';
    }
};

};