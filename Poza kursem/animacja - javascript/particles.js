var canvas = document.getElementById("particles"); //pobieramy element o id particles
var ctx = canvas.getContext('2d'); // pobieramy kontekst dla canvas - sposob rysowania
//okrelsamy stałe
var W = canvas.width = window.innerWidth;
var H = canvas.height = window.innerHeight;
//stworzenie pustej tablicy ktora zostanie zapelniona kuleczkami
var circles =[];
var midPointX = W/2;
var midPointY = H/2;
//grawitacja
var velocityX = 0;
var velocityY = 0.1;

function Ball(ctx) {
this.create = function (){
  //tworzenie nowej kuleczki
  //poczatkowa srednica kuleczkami
  this.radius = 2 + Math.random()*3;
  //połozenie
  this.x = midPointX;
  this.y = midPointY;
  //predkosc
  this.vx = 10* (Math.random()-0.5);
  this.vy = 10* (Math.random()-0.5);
  //przyspieszenie
  this.ax = (Math.random())*(velocityX);
  this.ay = (Math.random())*(velocityY);
  //kolor
  this.red = Math.round(Math.random())*255;
  this.green = Math.round(Math.random())*255;
  this.blue = Math.round(Math.random())*255;
}
//rysowanie pileczki
this.draw = function (){
  ctx.fillStyle = "rgba(" + this.red + ", " + this.green + ", " +this.blue +", 0.5)";
  ctx.beginPath (); //kladziemy pedzel
    ctx.arc(this.x,this.y, this.radius, 0, //kat poczatkowy
       Math.PI*2, false //kierunek
     );
     ctx.closePath (); //odlozenie pedzla
     ctx.fill();

}
this.move = function (){
  //zmiana polozenia - predkosc
  this.x += this.vx;
  this.y += this.vy;
  //zmiana polozenia - przyspieszenie
  this.vx += this.ax;
  this.vy += this.ay;
  // zmiana promienia kuleczki
  this.radius -= 0.01;
}
}
//zapelnienie tablicy kuleczkami
for (var i=0; i<100; i++){
  var ball = new Ball(ctx); //nowa instancja klasy Ball
  ball.create ();
  circles.push(ball); //zapisanie kuleczki do tablicy
}
function drawFrame(){
  ctx.fillStyle = "rgba(255,255,255,0.1)";
  ctx.fillRect(0, 0, W, H); //tworzymy plotno o konkretnych wymiarach
  circles.forEach(function (circle){
    if (circle.radius > 0 && circle.x <=W && circle.x >=0 && circle.y <=H && circle.y >=0){
      circle.draw();
      circle.move();

    }
    else{
      circle.create();
    }
  });
}
setInterval (drawFrame, 1000/60.0);
