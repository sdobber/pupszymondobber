1.1 Wyświetlić zawartość wszystkich kolumn z tabeli pracownik.
SELECT * FROM pracownik;

1.2 Z tabeli pracownik wyświetlić same imiona pracowników.
SELECT imie FROM pracownik;

1.3 Wyświetlić zawartość kolumn imię, nazwisko i dział z tabeli pracownik.
SELECT imie, nazwisko, dzial FROM pracownik;

2.1 Wyświetlić zawartość kolumn imię, nazwisko i pensja z tabeli pracownik. Wynik
posortuj malejąco względem pensji .
SELECT imie, nazwisko, pensja FROM pracownik ORDER BY pensja DESC;

2.2 Wyświetl zawartość kolumn nazwisko i imię z tabeli pracownik. Wynik posortuj
rosnąco (leksykograficznie) względem nazwiska i imienia.
SELECT nazwisko, imie FROM pracownik ORDER BY nazwisko ASC, imie ASC;

2.3 Wyświetlić zawartość kolumn nazwisko, dział, stanowisko z tabeli pracownik. Wynik
posortuj rosnąco względem działu, a dla tych samych nazw działów malejąco względem
stanowiska.
SELECT nazwisko, dzial, stanowisko FROM pracownik ORDER BY dzial ASC, stanowisko DESC;

3.1 Wyświetlić niepowtarzające się wartości kolumny dział z tabeli pracownik.
SELECT DISTINCT dzial FROM pracownik;

3.2 Wyświetlić unikatowe wiersze zawierające wartości kolumn dział i stanowisko w
tabeli pracownik.
SELECT DISTINCT dzial, stanowisko FROM pracownik;

3.3 Wyświetlić unikatowe wiersze zawierające wartości kolumn dział i stanowisko w
tabeli pracownik. Wynik posortuj rmalejąco względem działu i stanowiska.
SELECT DISTINCT dzial, stanowisko FROM pracownik ORDER BY dzial DESC, stanowisko DESC;

4.1 Znajdź pracowników o imieniu Jan. Wyświetl ich imiona i nazwiska.
SELECT imie, nazwisko FROM pracownik WHERE imie='JAN';			(imie musi być dużymi literami pisane, żeby dzialalo)

4.2 Wyświetlić imiona i nazwiska pracowników pracujących na stanowisku sprzedawca.
SELECT imie, nazwisko FROM pracownik WHERE stanowisko='SPRZEDAWCA';

4.3 Wyświetlić imiona, nazwiska, pensje pracowników, którzy zarabiają powyżej 1500 zł.
Wynik posortuj malejąco względem pensji.
SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja>1500 ORDER BY pensja DESC;

5.1 Z tabeli pracownik wyświetlić imiona, nazwiska, działy, stanowiska tych pracowników,
którzy pracują w dziale obsługi klienta na stanowisku sprzedawca.
SELECT imie, nazwisko, dzial, stanowisko FROM pracownik WHERE dzial='OBSLUGA' AND stanowisko='SPRZEDAWCA';

5.2 Znaleźć pracowników pracujących w dziale technicznym na stanowisku kierownika
lub mechanika. Wyświetl imie, nazwisko, dzial, stanowisko.
SELECT imie, nazwisko, dzial, stanowisko FROM pracownik WHERE dzial='TECHNICZNY' AND stanowisko='KIEROWNIK' OR stanowisko='MECHANIK';

5.3 Znaleźć samochody, które nie są marek fiat i ford.
SELECT marka FROM samochod WHERE NOT marka='FIAT' AND NOT marka='FORD';

6 Predykat IN
6.1 Znaleźć samochody marek mercedes, seat i opel.
SELECT marka FROM samochod WHERE marka IN('MERCEDES', 'SEAT', 'OPEL');

6.2 Znajdź pracowników o imionach Anna, Marzena, Alicja. Wyświetl ich imiona
nazwiska i daty zatrudnienia.
SELECT imie, nazwisko, data_zatr FROM pracownik WHERE imie IN ('ANNA', 'MARZENA', 'ALICJA');

6.3 Znajdź klientów, którzy nie mieszkają w Warszawie lub we Wrocławiu. Wyświetl ich
imiona, nazwiska i miasta zamieszkania.
SELECT imie, nazwisko, miasto FROM klient WHERE miasto NOT IN ('WARSZAWA', 'WROCLAW');


7 Predykat LIKE
7.1 Wyświetlić imiona i nazwiska klientów, których nazwisko zawiera literę K.
SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE '%K%';

7.2 Wyświetlić imiona i nazwiska klientów, dla których nazwisko zaczyna się na D, a
kończy się na SKI.
SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE 'D%SKI';

7.3 Znaleźć imiona i nazwiska klientów, których nazwisko zawiera drugą literę O lub A.
SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE '_O%' OR nazwisko LIKE '_A%';

8 Predykat BETWEEN
8.1 Z tabeli samochód wyświetlić wiersze, dla których pojemność silnika jest z przedziału
[1100,1600].
SELECT * FROM samochod WHERE poj_silnika BETWEEN 1100 AND 1600;

8.2 Znaleźć pracowników, którzy zostali zatrudnieni pomiędzy datami 1997-01-01 a
1997-12-31.
SELECT * FROM pracownik WHERE data_zatr BETWEEN '1997-01-01' AND '1997-12-31';

8.3 Znaleźć samochody, dla których przebieg jest pomiędzy 10000 a 20000 km lub
pomiędzy 30000 a 40000 km.
SELECT * FROM samochod WHERE przebieg BETWEEN '10000' AND '20000' OR przebieg BETWEEN '30000' AND '40000';

9 Wartość NULL
9.1 Znaleźć pracowników, którzy nie mają określonego dodatku do pensji.
SELECT * FROM pracownik WHERE dodatek IS NULL;

9.2 Wyświetlić klientów, którzy posiadają kartę kredytową.
SELECT * FROM klient WHERE nr_karty_kredyt IS NOT NULL;

9.3 Dla każdego pracownika wyświetl imię, nazwisko i wysokość dodatku. Wartość NULL
z kolumny dodatek powinna być wyświetlona jako 0. Wskazówka: Użyj funkcji COALESCE.
SELECT imie, nazwisko, COALESCE(dodatek, 0) AS dodatek FROM pracownik;

10 Kolumny wyliczeniowe (COALESCE)
10.1 Wyświetlić imiona, nazwiska pracowników ich pensje i dodatki oraz kolumnę
wyliczeniową do_zapłaty, zawierającą sumę pensji i dodatku. Wskazówka: Wartość NULL z
kolumny dodatek powinna być liczona jako zero.
SELECT imie, nazwisko, pensja, COALESCE(dodatek, 0) AS dodatek, pensja+COALESCE(dodatek, 0) AS do_zaplaty FROM pracownik;

10.2 Dla każdego pracownika wyświetl imię, nazwisko i wyliczeniową kolumnę
nowa_pensja, która będzie miała o 50% większą wartość niż dotychczasowa pensja.
SELECT imie, nazwisko, pensja*1.5 AS nowa_pensja FROM pracownik;

10.3 Dla każdego pracownika oblicz ile wynosi 1% zarobków (pensja + dodatek). Wyświetl
imię, nazwisko i obliczony 1%. Wyniki posortuj rosnąco względem obliczonego 1%.
SELECT imie, nazwisko, (pensja+COALESCE(dodatek, 0))*0.01 AS jeden_procent FROM pracownik ORDER BY jeden_procent ASC;

11 Złączenia wewnętrzne dwóch tabel
11.1 Wyszukaj samochody, który nie zostały zwrócone. (Data oddania samochodu ma
mieć wartość NULL.) Wyświetl identyfikator, markę i typ samochodu oraz jego datę
wypożyczenia i oddania.
SELECT s.id, s.marka, s.typ, w.data_wyp, w.data_odd FROM samochod s LEFT JOIN wypozyczenie w ON s.id=w.id_samochod WHERE data_wyp IS NOT NULL AND data_odd IS NULL;

11.2 Wyszukaj klientów, którzy nie zwrócili jeszcze samochodu. . (Data oddania
samochodu ma mieć wartość NULL.) Wyświetl imię i nazwisko klienta oraz identyfikator i
datę wypożyczenia nie
zwróconego jeszcze samochodu. Wynik posortuj rosnąco względem nazwiska i imienia
klienta oraz identyfikatorze i dacie wypożyczenia samochodu.
SELECT k.imie, k.nazwisko, w.id_samochod, w.data_wyp FROM klient k LEFT JOIN wypozyczenie w ON k.id=w.id_klient WHERE w.data_odd IS NULL AND w.data_wyp IS NOT NULL ORDER BY k.imie, k.nazwisko, w.id_samochod, w.data_wyp;

11.3 Dla każdego klienta wyszukaj daty i kwoty wpłaconych kaucji. Wyświetl imię i
nazwisko klienta oraz datę wpłacenia kaucji (ta sama data co data wypożyczenia
samochodu) i jej wysokość (różną od NULL).
SELECT k.imie, k.nazwisko, w.data_wyp, w.kaucja FROM klient k LEFT JOIN wypozyczenie w ON k.id=w.id_klient WHERE w.kaucja IS NOT NULL;

12 Złączenia wewnętrzne większej liczby tabel
12.1 Dla każdego klienta. który choć raz wypożyczył samochód, wyszukaj jakie i kiedy
wypożyczył samochody. Wyświetl imię i nazwisko klienta oraz markę i typ wypożyczonego
samochodu. Wynik posortuj rosnąco po nazwisku i imieniu klienta oraz marce i typie
samochodu.
SELECT k.imie, k.nazwisko, s.marka, s.typ, w.data_wyp FROM klient k INNER JOIN wypozyczenie w ON k.id=w.id_klient INNER JOIN samochod s ON s.id=w.id_samochod ORDER BY k.nazwisko, k.imie, s.marka, s.typ;

12.2 Dla każdej filii wypożyczalni samochodów (tabela miejsce) wyszukaj jakie
samochody były wypożyczane. Wyświetl adres filii (ulica i numer) oraz markę i typ
wypożyczonego samochodu. Wyniki posortuj rosnąco względem adresu filii, marki i typu
damochodu.
SELECT m.ulica, m.numer, s.marka, s.typ FROM miejsce m INNER JOIN wypozyczenie w ON m.id=w.id_miejsca_wyp INNER JOIN samochod s ON s.id=w.id_samochod WHERE w.data_wyp IS NOT NULL ORDER BY m.ulica, m.numer, s.marka, s.typ;

12.3 Dla każdego wypożyczonego samochodu wyszukaj informację jacy klienci go
wypożyczali. Wyświetl identyfikator, markę i typ samochodu oraz imię i nazwisko klienta.
Wyniki posortuj po identyfikatorze samochodu oraz nazwisku i imieniu klienta.
SELECT s.id, s.marka, s.typ, k.imie, k.nazwisko FROM samochod s INNER JOIN wypozyczenie w ON s.id=w.id_samochod INNER JOIN klient k ON k.id=w.id_klient WHERE w.data_wyp IS NOT NULL ORDER BY s.id, k.nazwisko, k.imie;

13 Funkcje agregujące bez grupowania
13.1 Znaleźć największą pensję pracownika.
SELECT MAX(pensja) FROM pracownik;

13.2 Znaleźć średnią pensję pracownika.
SELECT AVG(pensja) FROM pracownik;

13.3 Znaleźć najwcześniejszą datę wyprodukowania samochodu .
SELECT MIN(data_prod) FROM samochod;

14 Podzapytania nieskorelowane z użyciem funkcji agregujących bez
grupowania
14.1 Wyświetl imiona, nazwiska i pensje pracowników, którzy posiadają najwyższą pensją.
SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja=(SELECT MAX(pensja) FROM pracownik);

14.2 Wyświetl pracowników (imiona, nazwiska, pensje), którzy zarabiają powyżej średniej
pensji.
SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja>(SELECT AVG(pensja) FROM pracownik);

14.3 Wyszukaj samochody (marka, typ, data produkcji), które zostały wyprodukowane
najwcześniej.
SELECT marka, typ, data_prod FROM samochod WHERE data_prod=(SELECT MIN(data_prod) FROM samochod);

15 Podzapytania nieskorelowane z predykatem IN
15.1 Wyświetl wszystkie samochody (marka, typ, data produkcji), które do tej pory nie
zostały wypożyczone.
SELECT marka, typ, data_prod FROM samochod WHERE id NOT IN (SELECT id_samochod FROM wypozyczenie);


15.2 Wyświetl klientów (imię i nazwisko), którzy do tej pory nie wypożyczyli żadnego
samochodu. Wynik posortuj rosnąco względem nazwiska i imienia klienta.
SELECT imie, nazwisko FROM klient WHERE id NOT IN (SELECT id_klient FROM wypozyczenie) ORDER BY nazwisko ASC, imie ASC;

15.3 Znaleźć pracowników (imię i nazwisko), którzy do tej pory nie wypożyczyli żadnego
samochodu klientowi.
SELECT imie, nazwisko FROM pracownik WHERE id NOT IN (SELECT id_pracow_wyp FROM wypozyczenie);

16 Modyfikacja danych w bazie danych (UPDATE)
16.1 Pracownikom, którzy nie mają określonej wysokości dodatku nadaj dodatek w
wysokości 50 zł.
UPDATE pracownik SET dodatek=50 WHERE dodatek IS NULL;

16.2 Klientowi o identyfikatorze równym 10 zmień imię i nazwisko na Jerzy Nowak.
UPDATE klient SET imie='JERZY', nazwisko='NOWAK' WHERE id=10;

16.3 Podwyższyć o 10% pensję pracownikom, którzy zarabiają poniżej średniej.
UPDATE pracownik SET pensja=pensja+pensja*0.1 WHERE pensja<(SELECT AVG(pensja) FROM pracownik);

17 Usuwanie danych z bazy danych (DELETE)
17.1 Usunąć klienta o identyfikatorze równym 17.
DELETE FROM klient WHERE id=17;

17.2 Usunąć wszystkie informacje o wypożyczeniach dla klienta o identyfikatorze równym 17.
DELETE FROM wypozyczenie WHERE id_klient=17;

17.3 Usunąć klientów, którzy nie wypożyczyli żadnego samochodu.
DELETE FROM klient WHERE id NOT IN (SELECT id_klient FROM wypozyczenie);

18 Dodawanie danych do bazy danych (INSERT)
18.1 Dodaj do bazy danych klienta o identyfikatorze równym 21: Adam Cichy zamieszkały
ul. Korzenna 12, 00-950 Warszawa, tel. 123-454-321.
INSERT INTO klient (id, imie, nazwisko, ulica, numer, miasto, kod, telefon) VALUES (21, 'ADAM', 'CICHY', 'KORZENNA', 12, 'WARSZAWA', '00-950', '123-454-321');

18.2 Dodaj do bazy danych nowy samochód o identyfikatorze równym 19: srebrna skoda
octavia o pojemności silnika 1896 cm3 wyprodukowana 1 września 2012 r. i o przebiegu 5
000 km.
INSERT INTO samochod (id, marka, typ, data_prod, kolor, poj_silnika, przebieg) VALUES (19,'SKODA', 'OCTAVIA', '2012-09-01', 'SREBRNY', 1896, 5000);

18.3 Dopisz do bazy danych informację o wypożyczeniu samochodu o identyfikatorze 19
przez klienta o identyfikatorze 21 w dniu 28 października 2012 r. przez pracownika o
identyfikatorze 1 w miejscu o identyfikatorze 2. Została pobrana kaucja 4000 zł, a cena za
dzień wypożyczenia wynosi 500 zł.
INSERT INTO wypozyczenie (id,id_klient, id_samochod, id_pracow_wyp, id_miejsca_wyp, data_wyp, kaucja, cena_jedn) VALUES (26,21,19,1,2,'2012-10-28',4000,500);

19 Wybrane funkcje daty i czasu (DAY, MONTH, YEAR, GETDATE,
DATEDIFF)
19.1 Wyszukaj pracowników zatrudnionych w miesiącu maju. Wyświetl ich imiona,
nazwiska i datę zatrudnienia. Wynik posortuj rosnąco względem nazwiska i imienia.
SELECT imie, nazwisko, data_zatr FROM pracownik WHERE EXTRACT(MONTH FROM data_zatr)=5 ORDER BY nazwisko ASC, imie ASC;

19.2 Dla każdego pracownika (imię i nazwisko) wypisz ile już pracuje dni. Wynik posortuj
malejąco według ilości przepracowanych dni.
SELECT imie, nazwisko, DATE_PART('day',NOW()-data_zatr) AS liczba_dni FROM pracownik ORDER BY liczba_dni DESC;

19.3 Wyszukaj klientów (imiona i nazwiska), którzy nie oddali jeszcze samochodu.
Wyświetl jaki wypożyczyli samochód (marka i typ) i ile dni już go trzymają. Wynik posortuj
malejąco względem ilości dni.
SELECT k.imie, k.nazwisko, s.marka, s.typ, DATE_PART('day', NOW()-data_wyp) AS liczba_dni FROM klient k INNER JOIN wypozyczenie w ON k.id=w.id_klient INNER JOIN samochod s ON s.id=w.id_samochod WHERE w.data_odd IS NULL ORDER BY liczba_dni DESC;

20 Wybrane funkcje operujące na napisach (LEFT, RIGHT, LEN,
UPPER, LOWER, STUFF)
20.1 Wyświetl imię, nazwisko i inicjały każdego klienta. Wynik posortuj względem
inicjałów, nazwiska i imienia klienta.
SELECT imie, nazwisko, (LEFT(imie,1)||'.'||LEFT(nazwisko,1)||'.') AS inicjaly FROM klient;

20.2 Wyświetl imiona i nazwiska klientów w taki sposób, aby pierwsza litera imienia i
nazwiska była wielka, a pozostałe małe.
SELECT LEFT(imie,1)||LOWER(RIGHT(imie,LENGTH(imie)-1)) AS imie ,LEFT(nazwisko,1)||LOWER(RIGHT(nazwisko,LENGTH(nazwisko)-1)) AS nazwisko FROM klient;

20.3 Wyświetl imiona, nazwiska i numery kart kredytowych klientów. Każda z ostatnich
sześciu cyfr wyświetlanego numeru karty kredytowej klienta powinna być zastąpiona
znakiem x .
SELECT imie, nazwisko, LEFT(nr_karty_kredyt,LENGTH(nr_karty_kredyt)-6)||'xxxxxx' AS nr_karty_kredyt FROM klient;
