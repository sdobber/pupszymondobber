function division (a, b){
    return (b===0) ? null : (a/b);  //jeżeli b bedzie zero to zwroci null, a jak nie to zwroci a/b
}
function divisionByArgumentsArray (){ //arguments to tablica argumentow funkcji automatycznie tworzona
    return (arguments.length!==2 || arguments[1]==0) ? null : (arguments[0]/arguments[1]); //jezeli dlugosc jest inna niz 2 lub drugi argument jest zerem
                                                                                           //to return null a jak nie to zwróci wynik dzielenia
}

function sumArguments(){
    if (arguments.length==0){
        return null;
    }
    else
    {
        var sum=0;
    for (var i=0; i<arguments.length; i++){
        if(typeof(arguments[i])!=='number'){
           return 'Przynajmniej jeden z podanych argumentów nie jest liczbą.';
        }
        else
        {
            sum+=arguments[i];
        }
    }
    return sum;
    }
    
}

var simplePI = division(22,7);
console.log(simplePI);
console.log(divisionByArgumentsArray(15,5));
console.log(divisionByArgumentsArray(15));
console.log(divisionByArgumentsArray(15,0));
console.log(sumArguments(1,2,3,4));