var myModule = {
    myProperty : 'hello', //takie robienie pól to jest za pomocą iterałów
    myConfig : { //tu mamy obiekt w obiekcie
        useCaching : true,
        language : "PL"
    },
    say : function (){ //funkcja w obiekcie
        console.log('Hello, how are you?');
    },
    report : function (){
        console.log("Caching is "+((this.myConfig.useCaching) ? "enabled" : 'disabled'));
    },
    update : function(newConfig){
        if (typeof newConfig === 'object'){
        this.myConfig=newConfig;
        console.log('Language is '+this.myConfig.language);
    }
    }
};

//teraz nie tworzymy new coś tam, bo mamy już obiekt zrobiony


myModule.say();
myModule.report();
myModule.update({
    language : "BR",
    useCaching : false
});
myModule.report();
console.log(myModule.myConfig);

/* pola prywatne */

var testModule = (function (){
    var counter=0;
    return { //zwracamy obiekt
        increment : function (){
            console.log("Counter has been incremented. ("+ (counter+1) + ")");
            return ++counter;
        },
        reset : function (){
            counter = 0;
            console.log("Counter has been resetted.");
        }
    };
})(); //od razu wywolujemy funkcje i wtedy do testModule jest podstawiany wynik dzialania tej funkcji
testModule.increment();
testModule.increment();
testModule.reset();
testModule.increment();
console.log(testModule.counter); //jest niezdefiniowane, bo w zmiennej testModul jest wynik funkcji, czyli obiekt i odwolujemy sie do metod tego obiektu
                                 //jest to forma pokazania, ze counter jest polem prywatnym, sposob stworzenia pola prywatnego w jezyku nieobiektowym
                                 
                                 
/*revealing module pattern - funkcje prywatne*/

var myRevModule = (function (){
    var privateCounter = 0;
    function privateFunction (){
        privateCounter++;
    }
    function publicIncrement (){ //tu nie ma przecinkow miedzy funkcjami bo nie sa to literaly tak jak na poczatku
        privateFunction();
    }
    function publicFunction (){
        publicIncrement();
    }
    function publicGetCount (){
        return privateCounter;
    }
    return {
        start : publicFunction, //nie dajemy nawiasów po funkcjach i wtedy do start jest przypisana cala funkcja razem z nazwa, a nie wynik dzialania tej
                                //funkcji, tu jest przyklad ukrycia funckji prywatnej
        increment: publicIncrement,
        count: publicGetCount
    };
})();
myRevModule.start();
myRevModule.privateFunction();