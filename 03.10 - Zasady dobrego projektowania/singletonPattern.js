var singleton = (function (){
    var instance;
    function init(){
        function privateMethod(){
            console.log('privateMethod');
        }
        var privateVariable = 'privateVariable';
        var privateRandomNum = Math.random();
        return {
            publicMethod : function (){
                console.log('publicMethod');
            },
            publicProperty : 'publicProperty',
            getRandomNum : function (){
                return privateRandomNum;
            }
        };
    }
    return {
      getInstance : function (){
          if (!instance) {  //lazy initialization - jak zmienna jest pusta to tworzymy, a jak juz jest utworzone to zwracamy - chodzi o to by była
                            // tylko jedna instancja obiektu danej klasy
              instance = init();
          }
          return instance;
      }
    };
})();

var instanceOne = singleton.getInstance();
var instanceTwo = singleton.getInstance();

console.log (instanceOne.getRandomNum()===instanceTwo.getRandomNum());


// tworzenie obiektu captain i upewnianie się, że będzie tylko jedna instancja takiego obiektu

var captain = (function (){
    var oneCaptain;
    function init(){
        var nazwa = "Szymon Dobber";
        
        return {
            getCaptain : function (){
                return nazwa;
            }
        };
    }
        return{
            getOneCaptain : function (){
                if (!oneCaptain){
                    oneCaptain = init();
                }
                return oneCaptain;
    }
        }
    
})();
var captainOne = captain.getOneCaptain();
var captainTwo = captain.getOneCaptain();
console.log(captainOne.getCaptain()===captainTwo.getCaptain());