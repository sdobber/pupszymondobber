function Vehicle (vehicleType){
    this.vehicleType = vehicleType || "car";
    this.model = 'default';
    this.licence = '00000000';
}
var testInstance = new Vehicle ("Mercedes");
console.log(testInstance);
var lada = new Vehicle("Lada");
lada.setModel = function(modelName){
    this.model = modelName;         // mozna dodawac kolejne pola do danej instancji stworzonej na podstawie funkcji Vehicle
}
lada.setColor = function(color){
    this.color=color;
}
lada.setModel("314");
lada.setColor('red');

console.log(lada);
var testInstanceTwo = new Vehicle ("Porsche");
console.log(testInstanceTwo);

/* drugi sposób */

function Laptop (){
    this.cost = function(){ //poniewaz to jest funkcja to potem w dekoratorach tez do tego pola przypisujemy funkcję
        return 2000;
    }
    this.screenSize = function (){
        return 14.1;
    }
    this.screenType = function (){
        return 'Brak';
    }
    this.screenSurface = function (){
        return 'Brak';
    }
    
}
/* zmiana 3 pierwszych dekoratorow na fabryke dekoratorow*/

function decorateLaptop(laptop, hardware){
    var price =laptop.cost();
    var hardware = hardware;
    if (hardware=='memory'){
        laptop.cost = function (){
        return price + 150;
    }  
    }
    if (hardware=='graphicCard'){
        laptop.cost = function(){
        return price + 1000;
    }
    }
    if (hardware=='hardDrive'){
        laptop.cost = function(){
        return price + 1000;
    }
    }
}
// zmiana 3 pierwszych dekoratorow na fabryke dekoratorow z uwzglednieniem ceny
function decorateLaptopAndPrice(laptop, hardware, addPrice){
    var price =laptop.cost();
    var hardware = hardware;
    if (hardware=='memory'){
        laptop.cost = function (){
        return price + parseFloat(addPrice);
    }  
    }
    if (hardware=='graphicCard'){
        laptop.cost = function(){
        return price + parseFloat(addPrice);
    }
    }
    if (hardware=='hardDrive'){
        laptop.cost = function(){
        return price + parseFloat(addPrice);
    }
    }
}
//wersja z zajęć
function decorateLaptopByComponents(laptop, type, componentPrice){
    var price = laptop.cost();
    laptop.cost = function (){
        return price +parseFloat(componentPrice);
    }
    laptop.components = laptop.components || []; //jak usuniemy 'laptop.components' z prawej strony to wtedy będzie tablica od nowa nadpisywana z inny                                                      //jednym elementem
    laptop.components.push(type);
}
/* pierwszy dekorator*/

//function memory (laptop){
//    var price = laptop.cost();
//    laptop.cost = function (){
//        return price + 150;
//    }
//}
//
///*drugi dekorator*/
//
//function graphicCard(laptop){
//    var price = laptop.cost();
//    laptop.cost = function(){
//        return price + 1000;
//    }
//}
///* trzeci dekorator*/
//function hardDrive (laptop){
//    var price = laptop.cost();
//    laptop.cost = function(){
//        return price + 1000;
//    }
//}
/*czwarty dekorator*/
function screenTypeLED (laptop){
    laptop.screenType = function (){
        return 'LED';
    }
}
/*piąty dekorator*/
function screenTypeTFT (laptop){
    
    laptop.screenType = function (){
        return 'TFT';
    }
}
/*szósty dekorator*/
function screenTypeLCD (laptop){
    
    laptop.screenType = function (){
        return 'LCD';
    }
}
/*siódmy dekorator*/
function screenSurfaceMatt (laptop){
    
    laptop.screenSurface = function (){
        return 'Matowa';
    }
}
/*ósmy dekorator*/
function screenSurfaceGloss (laptop){
    
    laptop.screenSurface = function (){
        return 'Błyszcząca';
    }
}
var myLaptop = new Laptop();
//memory(myLaptop);
console.log(myLaptop.cost());
//graphicCard(myLaptop);
console.log(myLaptop.cost());
//hardDrive(myLaptop);
console.log(myLaptop.cost());
screenTypeLED(myLaptop);
console.log(myLaptop.screenType());
screenTypeTFT(myLaptop);
console.log(myLaptop.screenType());
screenTypeLCD(myLaptop);
console.log(myLaptop.screenType());
screenSurfaceMatt(myLaptop);
console.log(myLaptop.screenSurface());
screenSurfaceGloss(myLaptop);
console.log(myLaptop.screenSurface());
decorateLaptop(myLaptop,'graphicCard');
console.log(myLaptop.cost());
decorateLaptopAndPrice(myLaptop, 'memory', 400);
console.log(myLaptop.cost());

decorateLaptopByComponents(myLaptop, 'memory', 150);
console.log(myLaptop.cost(), myLaptop.components);
decorateLaptopByComponents(myLaptop, 'graphicCard', 1000);
console.log(myLaptop.cost(), myLaptop.components);
decorateLaptopByComponents(myLaptop, 'hardDrive', 1000);
console.log(myLaptop.cost(), myLaptop.components);

//zadanie z setterem i getterem
var test =(function(){
	var myField;
        var myColor;
	function setMyField(arg){
		myField=arg;
	}
        function setMyColor(arg){
                myColor=arg;
        }
	function getMyField(){
		return myField;
	}
        function getMyColor(){
                return myColor;
        }
	return {
		setMyField:setMyField,
                setMyColor:setMyColor,
		getMyField:getMyField,
                getMyColor:getMyColor
	}
	
})();
console.log(test.myField);
test.setMyField(5);
console.log(test.getMyField());
test.setMyColor('red');
console.log(test.getMyColor());