//tworzenie obiektu za pomoca fabryki obiektow
function Car(model, year, mileage){
    this.model = model;
    this.year = year;
    this.mileage = mileage;
    
    this.toString = function(){
        return this.model +" (" + this.year + ") passed " +this.mileage + "km.";
    }
}
var civic = new Car ("Honda Civic", 2009, 20000),
    mondeo = new Car ("Ford Mondeo", 2016, 5000);
    
console.log(civic.toString());
console.log(mondeo.toString());


//tworzenie obiektu z rozszerzeniem funkcjonalnosci poprzez prototyp wtedy każdy obiekt ma ten prototyp, nawet jak zdefiniowalismy pozniej ten prototyp
function Figure (type, area, circumference){
    this.type = type;
    this.area = area;
    this.circumference = circumference;
}   

Figure.prototype.toString = function(){
        return 'Ta figura to ' + this.type + ' o powierzchni ' + this.area + ' i obwodzie ' +this.circumference;
}


var kwadrat = new Figure ('kwadrat', 4, 8);
var prostokat = new Figure ('trójkąt', 6, 12);
console.log(kwadrat.toString());
console.log(prostokat.toString());