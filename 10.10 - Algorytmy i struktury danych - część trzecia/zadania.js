//Napiszmy symulator banku. Stwórzmy obiekt, który będzie realizował wpłaty, wypłaty do banku, pozwoli się na zalogowanie za pomocą nr PESEL. Do przetrzymywania informacji nt wpłat i wypłat użyj odpowiedniej struktury danych (stos / kolejka). Symulator banku powinien zawierać pola pozwalające na logowanie, następnie po zalogowaniu na wpłatę, wypłatę, przejrzenie logów wpłat i wypłat.
//
//Bank powinien pozwolić na wcześniejsze zarejestrowanie klienta (można stworzyć kilku predefiniowanych do testów). Klienci powinni być przetrzymywani we wcześniej utworzonym obiekcie (tj. strukturze danych Klient)

$(document).ready(function(){
    
    
    var Stack = function(){
    this.elems = [];
    this.empty = function (){
        return (this.elems.length===0) ? true : false;
    };
    this.pop = function (){
        if(this.elems.length===0){
            return null;
        }
        else {
            return this.elems.pop();
        }
    };
    this.push = function (arg){
        this.elems.push(arg);
    };
    this.size = function (){
        return this.elems.length;
    };
    this.top = function (){
        if(this.elems.length===0){
            return null;
        }
        else{
        return this.elems.length-1;
    }
    };
};

var Klient = function(opt) {
	this.imie = opt.imie || '';
	this.nazwisko = opt.nazwisko || '';
	this.PESEL = opt.PESEL || '';
	this.stanKonta = opt.stanKonta || 0.0;
	this.historia = new Stack();
	
	this.wplac = function(kwota) {
		this.stanKonta += Math.abs(kwota);
		this.historia.push(Math.abs(kwota));
	}
	
	this.wyplac = function(kwota) {
		this.stanKonta -= Math.abs(kwota);
		this.historia.push(-1 * Math.abs(kwota));
	}
	
}
var klienci = {};
klienci[54122912875] = new Klient({
	imie: 'Pawel',
	nazwisko: 'Wal',
	PESEL: 54122912875
});
klienci[90082710559] = new Klient({
	imie: 'Szymon',
	nazwisko: 'Dobber',
	PESEL: 90082710559
});

    $('#rejestracja').on('click', function(){
        $(document).find('#stan_rejestracji').removeClass('ukrycie');
    });
    $('#loguj').on('click', function(){
        
            if(klienci[$(document).find('#pesel').val()]){
            $(document).find('#stan_konta').removeClass('ukrycie');
            $(document).find('#moneyAmount').html(klienci[$('#pesel').val()].stanKonta);
			$('#rejestracja').attr('disabled','disabled');
            }
            else {
                alert ('Taki pesel nie jest zarejestrowany.');
            }
        
    });
    $('#wyloguj').on('click', function(){
        $(document).find('#stan_konta').addClass('ukrycie');
        $('#pesel').val(null);
        $('#historiahtml').addClass('ukrycie');
        $('#wplac_ilosc').val(null);
        $('#wyplac_ilosc').val(null);
		$('#rejestracja').prop('disabled',false);
    });
    $('#zarejestruj').on('click', function(){
        
        
            if(klienci[$('#register_pesel').val()]){
                $(document).find('#stan_rejestracji').addClass('ukrycie');
                return alert ('Taki klient już istnieje.');
            }
            else {
                klienci[$('#register_pesel').val()]= new Klient({
                    imie:$('#imie').val(),
                    nazwisko:$('#nazwisko').val(),
                    PESEL:$('#register_pesel').val(),
                });
            }
        console.log(Object.keys(klienci));
        console.log($('#imie').val(), $('#register_pesel').val());
		$('#imie').val(null);
		$('#nazwisko').val(null);
        $(document).find('#stan_rejestracji').addClass('ukrycie');
    });
    $('#wplac').on('click', function(){
        klienci[$('#pesel').val()].wplac(parseInt($(document).find('#wplac_ilosc').val()));
        $(document).find('#moneyAmount').html(klienci[$('#pesel').val()].stanKonta);
        console.log(klienci[$('#pesel').val()].stanKonta);
    });
    $('#wyplac').on('click', function(){
        klienci[$('#pesel').val()].wyplac(parseInt($(document).find('#wyplac_ilosc').val()));
        $(document).find('#moneyAmount').html(klienci[$('#pesel').val()].stanKonta);
        console.log(klienci[$('#pesel').val()].stanKonta);
    });
    $('#historia').on('click', function(){
        var tempHist = Object.create(klienci[$('#pesel').val()].historia);
        var histSize = tempHist.size();
        $('<p/>').html('Historia').appendTo('#historiahtml');
        for(var i = 0; i < histSize; i++) {
                var tmpOper = tempHist.pop();
                if(tmpOper < 0 ) $('<pre/>').html("Wypłacono: " + tmpOper + "PLN").appendTo('#historiahtml');
                else $('<pre/>').html("Wpłacono: " + tmpOper + "PLN").appendTo('#historiahtml');
        }
		$('#historiahtml').removeClass('ukrycie');
    });
    console.log(Object.keys(klienci));
    console.log($('#imie').val(), $('#register_pesel').val());
});