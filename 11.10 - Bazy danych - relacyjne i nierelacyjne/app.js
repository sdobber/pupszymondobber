$(document).ready(function() {

	$('button').on('click', function () {
		var imie = $('#imie').val();
		var nazwisko = $('#nazwisko').val();
		var action = $(this).attr('id');
		$.ajax({
			url: 'json.php',
			dataType: 'json',
			data:{imie:imie, nazwisko:nazwisko, action:action},//wysylamy do php
			success: function(data) {
				/*
				jezeli nacisnieto przycisk zapisz to w divie .info umieśćmy
				na zielono tekst 'zapisano'
				w przeciwnym wypadku jezeli nacisnieto wczytaj, umiescmy liste
				uzytkownikow wczytana z bazy.
				*/
				if(action=='save'){
					$('.info').html('Zapisano').attr('style','color:green');
				}
				else{
					$('.info').html('').attr('style','color:black');
					$('<ul/>',{class:'lista'}).appendTo('.info');
					for(var i=0; i<data.users.length; i++){
						$('<li/>').html(data.users[i]).appendTo('.lista');
					}
				}
			}
		});
	});
});
/*$(document).ready(function() {

	$('.download').on('click', function () {
		
		$.ajax({
			url: 'json.php',
			dataType: 'json',
			success: function(data) {
				$('<ul/>',{class:'lista'}).appendTo('.info');
				data.users.sort();
				for(var i=0; i<data.users.length; i++){
					$('<li/>').html(data.users[i]).appendTo('.lista');
				}
				//$(document).find('.info').html(data.klucz+data.klucz2); trzeba utworzyc tablice klucz - wartosc
			}
		});
		
	});

});*/
