#-*- coding: utf-8 -*-
import random
# można print pisać w formie print("napis", zmienna,"napis");
# jezeli dajemy *data_set, ktore jest tablica jako argument funkcji i wtedy on rozpakowuje poszczególne elementy tablicy do odpowiednich elementow

#w funkcji warto dać coś do argumentu funkcji, jezeli nie zostanie wpisany argument funkcji
# import nazwa - wczytuje inny plik do programu, i wtedy w głównym programie wywolujemy nazwa_modulu.nazwa_funkcji
"""
def check_gender(sex='nieznana'):
    if sex is 'k':
        sex = "kobieta"
    if sex is 'm':
        sex = "mężczyzna"
    print sex

check_gender('k')
check_gender('m')
check_gender('d')
check_gender()
"""

# argument - liczba graczy
# tablica z wyborami
# do ilu wygranych
# wagi wyborów
"""
liczba_graczy = raw_input("Podaj liczbę graczy. ")
wybory = ["Papier", "Nożyce", "Kamień"]
wagi = [3,2,1]
ile_wygranych = raw_input("Do ilu wygranych. ")
wygrane =[]
wyniki=[]
def gra(liczba_graczy, ile_wygranych):
    global wygrane
    for i in range(int(liczba_graczy)):
        wygrane.append(0)
        wyniki.append(0)
    while any(wynik == int(ile_wygranych) for wynik in wygrane) == False:
        for i in range(int(liczba_graczy)):
            indeks = random.randrange(len(wybory))
            wybor = wybory[indeks]
            waga = wagi[indeks]
            wyniki[i]=waga
        if wyniki.count(max(wyniki)) == 1:
            indeks_wygranego = wyniki.index(max(wyniki))
            wygrane[indeks_wygranego] = wygrane[indeks_wygranego]+1
            print "Wygrał gracz nr {:d}".format(indeks_wygranego+1)
        else:
            print "Nikt nie wygrał"
        print "\n"
    krol_rozgrywek = wygrane.index(int(ile_wygranych))
    print "Królem rozgrywek został gracz nr {:d}".format(krol_rozgrywek+1)
            
gra(liczba_graczy, ile_wygranych)
"""
"""
lista_zakupow = ["chleb", "mleko", "czosnek", "woda", "masło", "pomidory", "bułki", "cebula"]
set = {"chleb", "masło", "woda"} #usuwa duplikaty
if "chleb" in lista_zakupow:
    print "Jest na liście"
if "woda" in set:
    print "Jest w secie"
"""
"""
from datetime import datetime
now = datetime.now()
print now

print now.year
print now.month
print now.day
print now.hour
print now.minute
print now.second
print now.milisecond
"""
import requests
from bs4 import BeautifulSoup

def trade_spider(max_pages):
    page = 1
    while page <max_pages:
        url = 'http://www.filmweb.pl/forum/muzyka/Oce%C5%84+i+zaproponuj+XXV,2785418?page='+str(page)
        source_code = requests.get(url)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text,"lxml")
        for link in soup.findAll('link'):
            href = link.get('href')
            print(href)
        page +=1
trade_spider(4)