# -*- coding: utf-8 -*-
import math
import random
tablica = ["papier", "kamień", "nożyce"]
playerWinCounter = 0
computerWinCounter = 0
print "1. Do trzech wygranych.\n"
print "2. Wprowadź ilość rund.\n"
gameOption = input("Wybierz opcję gry. ")
if (gameOption==1):
    z = 3
    while(playerWinCounter < z and computerWinCounter < z):
        x = input("Podaj swoją opcję, gdzie 0-papier, 1-kamień, 2-nożyce. ")
        y = random.randrange(0,3)
        if (tablica[x]==tablica[y]):
            print "Remis"
            print tablica[x]
            print tablica[y]
        if (x==0 and y==1 or x==1 and y==2 or x==2 and y==0):
            print "Wygrałeś"
            print tablica[x]
            print tablica[y]
            playerWinCounter+=1
        if (x==0 and y==2 or x==1 and y==0 or x==2 and y==1):
            print tablica[x]
            print tablica[y]
            print "Wygrał komputer"
            computerWinCounter+=1
    if (playerWinCounter == z):
        print "Wygrałeś %i razy. Koniec Gry." %(z)
    if (computerWinCounter == z):
        print "Komputer wygrał %i razy. Koniec Gry." %(z)
if (gameOption==2):
    z = input("Podaj do ilu wygranych chcesz zagrać. ")
    while(playerWinCounter < z and computerWinCounter < z):
        x = input("Podaj swoją opcję, gdzie 0-papier, 1-kamień, 2-nożyce. ")
        y = random.randrange(0,3)
        if (tablica[x]==tablica[y]):
            print "Remis"
            print tablica[x]
            print tablica[y]
        if (x==0 and y==1 or x==1 and y==2 or x==2 and y==0):
            print "Wygrałeś"
            print tablica[x]
            print tablica[y]
            playerWinCounter+=1
        if (x==0 and y==2 or x==1 and y==0 or x==2 and y==1):
            print tablica[x]
            print tablica[y]
            print "Wygrał komputer"
            computerWinCounter+=1
    if (playerWinCounter == z):
        print "Wygrałeś %i razy. Koniec Gry." %(z)
    if (computerWinCounter == z):
        print "Komputer wygrał %i razy. Koniec Gry." %(z)
if (gameOption != 1 and gameOption!=2):
    print "Nie wybrałeś odpowiedniej opcji gry."

