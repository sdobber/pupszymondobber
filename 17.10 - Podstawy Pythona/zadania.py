# -*- coding: utf-8 -*-
import math
#print "To jest jakiś program" - zakomentowane
#zmienna='cos'
#print zmienna
"""
komentarz wielolinijkowy
"""
#zmienna = raw_input("Podaj liczbę\n")
#print zmienna

"""
len() - dlugosc stringa
lower() - zmienia na male
upper() - zmienia na duze
str() - zmienia liczbe na string
"""
"""
x = "To jest liczba"
x="10"
y = 10
print x + str(y)
print x, y
print type(x)
print type(y)
print type(str(y))
print type(int(x)) float() tez dziala
"""
"""
zmienna = 3.141592
print round(zmienna,2) #wieksze zajecie procesora
print "To jest PI %0.20f" % (zmienna)
#albo wersja dla 3.0 zalecane
print "To jest PI {:.2f}".format(zmienna)
"""
"""
max()
min()
abs()
type()
"""
"""
print max(3,4,5)
print min(3,4,5)
print abs(-2)

a = int(raw_input("Podaj pierwszą liczbę: "))
b = int(raw_input("Podaj drugą liczbę: "))
def funkcja():
    wynik=a**b
    return wynik
print funkcja()
"""
#if elif else
"""
w=10.2332
if isinstance(w,float):
    print "Jest przecinek"
else:
    print "Nie ma przecinka"
"""
"""
def czy_liczba(a):
    #mamy typ numeryczny
    if unicode(a, 'utf-8').isnumeric() == True:
        return True
    #mamy stringa, bo wystepuja litery
    elif any(c.isalpha() for c in a):
        return False
    elif any(... for c in a):
        return False
    else:
        return True

def potegowanie():
    x = raw_input("Podaj X: ")
    y = raw_input("Podaj Y: ")
    
    if czy_liczba(x) == True and czy_liczba(y) == True:
        #wykonujemy potegowanie
        return float(x)**float(y)
    else:
        #wyswietlamy
        return str(x)+"^"+str(y)

print potegowanie()
"""
"""
try:
    a = float(a)
    b = float(b)
    print a**b
except ValueError:
    print str(a) + "^"+ str(b)
"""

