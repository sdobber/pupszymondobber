//implementacja stosu
var Stack = function(){
    this.elems = [];
    this.empty = function (){
        return (this.elems.length===0) ? true : false;
    };
    this.pop = function (){
        if(this.elems.length===0){
            return null;
        }
        else {
            return this.elems.pop();
        }
    };
    this.push = function (arg){
        this.elems.push(arg);
    };
    this.size = function (){
        return this.elems.length;
    };
    this.top = function (){
        if(this.elems.length===0){
            return null;
        }
        else{
        return this.elems.length-1;
    }
    };
    /*
     tu można dodać return{
                          empty:empty,
                          push:push,
                          i tak dalej
     */
};

var myStack = new Stack();

myStack.push(5);
myStack.push(9);
myStack.size();
myStack.push(11);
myStack.top();
myStack.pop();
myStack.pop();
myStack.pop();
myStack.empty();


//stos w modulowym wzorcu projektowym gdzie elems jest zmienna prywatna

var Stack = (function(){
    var elems = [];
    return{
    	empty : function (){
	        return (elems.length===0) ? true : false;
	    },
	    pop : function (){
	        if(elems.length===0){
	            return null;
	        }
	        else {
	            return elems.pop();
	        }
	    },
	    push : function (arg){
	        elems.push(arg);
	    },
	    size : function (){
	        return elems.length;
	    },
	    top : function (){
	    	if (elems.length===0){
	    		return null;
	    	}
	    	else{
	        return elems.length-1;
	    	}
	    }
    }
})();

console.log(Stack.top());
Stack.push(5);
Stack.push(9);
Stack.size();
Stack.push(11);
Stack.pop();
Stack.pop();
Stack.pop();
Stack.empty();
console.log(Stack.elems);


//implementacja kolejki

function Queue (s){
    this.elems=[];
    this.head=null;
    this.tail=0;
    this.maxSize = s || 100;
}
Queue.prototype.pop = function(){
	if(this.elems.length===0){
		return null;
	}
	else{
    this.head = (this.elems.length-1 === 0) ? null : 0;
    this.tail = this.elems.length-1;
    return this.elems.shift();
	}
}
Queue.prototype.empty = function(){
    return (this.elems.length===0) ? true : false;
}


Queue.prototype.push = function(arg){
        if(this.maxSize==this.elems.length){
            return 'Osiągnięto maksymalną wielkość kolejki.';
        }
        else{
            this.elems.push(arg);
            this.head = (this.elems.length === 0) ? null : 0;
            this.tail = this.elems.length;
        }
}
Queue.prototype.size = function(){
    return this.elems.length;
}
var myQueue = new Queue();



//Napisz skrypt który dokona obliczeń, dla w
//prowadzonego ciągu znaków w odwrotnej notacji 
//polskiej.
//Informacje nt Odwrotnej notacji polskiej:
//
//https://pl.wikipedia.org/wiki/Odwrotna_notacja_polska
//
//do przetrzymywania danych użyj klasy stosu

var Stack = function(){
    this.elems = [];
    this.empty = function (){
        return (this.elems.length===0) ? true : false;
    }
    this.pop = function (){
        if(this.elems.length===0){
            return null;
        }
        else {
            return this.elems.pop();
        }
    }
    this.push = function (arg){
        this.elems.push(arg);
    }
    this.size = function (){
        return this.elems.length;
    }
    this.top = function (){
        if(this.elems.length===0){
            return null;
        }
        else{
        return this.elems.length-1;
    }
    }
    
}
function count(){
    var myStack = new Stack();
    var wynik=0;
        var tab = arguments[0].split(' ');
        for(var i=0; i<tab.length; i++){
            if(tab[i]!='+'&&tab[i]!='-'&&tab[i]!='*'&&tab[i]!='/'){
                myStack.push(parseInt(tab[i]));
            }
            else {
                switch(tab[i]){
                    case '+':
                        wynik=myStack.pop()+myStack.pop();
                        myStack.push(wynik);
                        break;
                    case '-':
                    	var b=myStack.pop();
                    	var a=myStack.pop();
                        wynik=a-b;
                        myStack.push(wynik);
                        break;
                    case '*':
                        wynik=myStack.pop()*myStack.pop();
                        myStack.push(wynik);
                        break;
                    case '/':
                        var b=myStack.pop();
                        if(b==0){
                            return 'Nie dzielimy przez 0';
                        }
                    	var a=myStack.pop();
                        wynik=a/b;
                        myStack.push(wynik);
                        break;
                }
            }
        }
        return wynik;
}
count('12 2 3 4 * 10 5 / + * +');


//funkcja przyjmuje string jako argument, uzywajac stosu, sprawdz czy przekazany argument jest palindromem.

var Stack = function(){
    this.elems = [];
    this.empty = function (){
        return (this.elems.length===0) ? true : false;
    }
    this.pop = function (){
        if(this.elems.length===0){
            return null;
        }
        else {
            return this.elems.pop();
        }
    }
    this.push = function (arg){
        this.elems.push(arg);
    }
    this.size = function (){
        return this.elems.length;
    }
    this.top = function (){
        if(this.elems.length===0){
            return null;
        }
        else{
        return this.elems.length-1;
    }
    }
    
}
function isItAPalindrome(){
    var myStack = new Stack();
    
        var tab = arguments[0].split('');
        for(var i=0; i<tab.length; i++){
            myStack.push(tab[i]);
        }
        for (var i=0; i<myStack.size()-1; i++)    {
        	if(tab[i]==myStack.pop()){
        		return 'Słowo jest palindromem.';
        	}
        	else{
        		return "Słowo nie jest palindromem.";
        	}
        }
}
isItAPalindrome('ok');



//implementacja listy
var List = function (s) {
   var elems = [];
   var maxSize = s || 100;

   function push_front(arg) {//dodaje element na poczatku listy
       arg=arg.toString();
       elems.unshift(arg);
      
   }
   
   function push_back(arg) {//dodaje element na końcu listy
       arg=arg.toString();
       elems.push(arg);
       
   }
   
   function insert(arg, arg2) {//dodaje element we wskazanym miejscu listy
       arg2=arg2.toString();
       elems.splice(arg, 0, arg2);//arg - miejsce w ktorym wstawiamy(index), 0 - opcjonalna ilosc elementow, ktore usuniemy, arg2 - to co wstawiamy
       
   }
   
   function pop_front() {//usuwa element z początku listy
       return (elems.length === 0) ? null :  elems.shift();
   }
   
   function pop_back() {//usuwa element z końca listy
       return (elems.length === 0) ? null : elems.pop();
   }
   
   function size() {//zwraca liczbe elementow na liscie
       return (elems.length === 0) ? null : elems.length;
   }
   
   function max_size() {//zwraca maksymalna liczbe elementow jakie moze pomieścić lista
       return (elems.length===0) ? null : maxSize;
   }
   
   function empty() {//sprawdza czy lista jest pusta
      return (elems.length === 0); //bo w sumie to zwraca true albo false odpowiednio do warunku
   }
   
   function remove(arg) {//usuwa z listy wszystkie elementy mające daną wartość
       for (var i = 0; i<elems.length;i++){
           if(elems[i] == arg){
               elems.splice(i, 1);
               --i;
           }
       }
       
   }
   
   function sort() {//układa elementy na liście rosnąco
       elems.sort();
       
   }
   
   function reverse() {//odwraca kolejność elementów na liście
       elems.reverse();
      
   }
   function showOne(arg){
       return elems[arg];
   }
   function show(){
       return elems;
   }
   return {
       push_front : push_front,
       push_back : push_back,
       insert : insert,
       pop_front : pop_front,
       pop_back : pop_back,
       size : size,
       max_size : max_size,
       empty : empty,
       remove : remove,
       sort : sort,
       reverse : reverse,
       showOne : showOne,
       show : show
   };
};
var myList = new List();
myList.push_front(4);
myList.push_front(6);
myList.push_back(8);
myList.insert(1,3);
myList.pop_front();
myList.pop_back();
console.log(myList.size());
console.log(myList.max_size());
console.log(myList.empty());
myList.push_front(4);
myList.push_front(6);
myList.push_back(8);
myList.insert(1,3);
myList.remove(4);
myList.sort();
myList.reverse();
console.log(myList.elems);


//zadeklarujmy tablice zawierajaca 10 uczniow (jako tablica stringow, zawierajaca imiona i nazwiska), a nastepnie napiszmy funkcje, ktora wykorzystujac liste, doda do niej wszystkich uczniwo, i zwroci liste, na zwroconej liscie posortujmy uczniow alfabetycznie, a nastepnie wyświetlmy co drugiego ucznia w konsoli w odwroconej kolejnosci
var tab = ['Ania Kowalska', 'Bogdan Markowski', 'Człowiek Ludzki', 'Pomidor Banan', 'Imię Nazwisko', 'Zenon Frankowski', 'Grzegorz Fajny', 'Dorota Dorota', 'Maria Maria', 'Michał Nowak'];
function useList (arg){
	for(var i=0; i<arg.length; i++){
		myList.push_back(arg[i]);
	}
	return myList;
}
useList(tab);
myList.sort();
myList.reverse();
for(var i=1; i<tab.length; i=i+2){
	console.log(myList.showOne(tab.length-i));
}
// używanie chainingu - w zwiazku z tym do niektorych funckji List dodaliśmy return this, by chaining byl mozliwy
var List = function(arg) {
	var data = [];
	var maxSize = arg || 100;
	
	this.push_front = function(arg) {
		if(data.length < maxSize) {
			data.unshift(arg.toString());
			return this;
		} else {
			return "Limit exceeded.";
		}
	}
	
	this.push_back = function(arg) {
		if(data.length < maxSize) {
			data.push(arg.toString());
			return this;
		} else {
			return "Limit exceeded.";
		}
	}
	
	this.insert = function(i, arg) {
		if(data.length < maxSize) {
			data.splice(i, 0, arg.toString());
			return this;
		} else {
			return "Limit exceeded.";
		}
	}
	
	this.pop_front = function() {
		return (data.length > 0) ? data.shift() : null; 
	}
	
	this.pop_back = function() {
		return (data.length > 0) ? data.pop() : null;
	}
	
	this.size = function() {
		return data.length;
	}
	
	this.max_size = function() {
		return maxSize;
	}
	
	this.empty = function() {
		return (data.length === 0);
	}
	
	this.sort = function() {
		data.sort();
		return this;
	}
	
	this.reverse = function() {
		data.reverse();
		return this;
	}
	
	this.remove = function(arg) {
		for(var i = 0; i < data.length; i++) {
			if(data[i] === arg.toString()) {
				data.splice(i, 1);
				--i;
			}
		}
		return this;
	}
	this.show = function(){
       return data;
   }
}


var list = new List();

var students = list.push_back('zenon').push_back('ania').push_back('pawel').sort().show();
console.log(students);


/*implementacja słownika
  hasValue() - metoda sprawdzająca czy występuje zadana wartość w słowniku - jeżeli występuje to zwracamy tablice kluczy, które mają taką wartość
  hasKey() - moteda sprawdza czy występuje dany klucz, zwraca false lub wartość klucza
  add() - możemy przekazać tablicę ['klucz', 'wartosc']
        - możemy przekazać dwa argumenty add(klucz, wartosc)
        - mozemy przekazać obiekt {klucz : wartosc}
        - metoda add() nie może dodać ponownie danego klucza jeśli taki występuje
        do zbadania możemy użyć: console.log(arr instanceof Array) - jeśli zwróci true, obiekt jest tablicą, jeśli nie - dziedziczy po obiekcie
  remove (key) - usuwa podany klucz ze słownika
  update(key, value) - metoda nadpisuje podany klucz, zadaną wartością, tylko wtedy, gdy dany klucz występuje w słowniku
  next(key) - metoda wyszukuje zadany klucz, a następnie zwraca jego następnik jako object { k: 'v' }
  prev(key) - metoda wyszukuje zadany klucz, a następnie zwraca jego poprzednik jako object { k: 'v' }
  value(key) - metoda zwraca wartość dla zadanego klucza
  show() - zwraca słownik w postaci tablicy zawierające tablice
  removeByValue(value) - usuwa klucze, które mają podaną wartość; można użyć delete dict['k'], bo delete usuwa wszystko razem z kluczem, ale delete w tablicy pozostawia indeks
*/
var Dictionary = function(arg) {
	var elems = {};
	var maxSize = arg || 100;
		function hasValue(arg) {
			if(Object.keys(elems).length===0){
    	             return null;
    	         }
    	          else{
    	             var tab =[];
        	            for(var i in elems){
            	        	if(arg==elems[i]){
                	    		tab.push(i);
                    		}
	                 }
    	                return tab;
        	        }
		}

        function hasKey(arg) {
        	if(Object.keys(elems).length===0){
                    return null;
                }
                else{
                	
                  	if(elems[arg]){
                    	return elems[arg];
                    }
                    else{
                    	return false;
                    }
                }
		}
		
 
        function add(arg, arg2) {
        	if (arguments.length===0){
        		return 'Podaj argumenty funkcji.';
        	}
        	if(arguments.length===1){
        		if(arg instanceof Array){
        			for (var i in arg){
        				if(elems[arg[i]]){
        					return 'Podany klucz istnieje.';
        				}
        				else{
        					elems[arg[0]]=arg[1];
        				}
        			}
        		}
        	}
        		if (arg instanceof Object === true && arg instanceof Array === false){
        		
        			for (var i in arg){
        				
        				if(elems[i]){
        					return 'Podany klucz istnieje.';
        				}
        				else{
        					for (var i in arg){
        						elems[i]=arg[i];
        					}
        				}
        			}
        			
        		}
        
        	if (arguments.length===2){
        		
        			if(elems[arg]){
        				return 'Podany klucz już istnieje.';
        			}
        			else{
        				elems[arg]=arg2;	
        			}
        		}
        }
        function remove(arg) {
            delete elems[arg];
        }
        function update(arg,arg2) {
            elems[arg]=arg2;
        }
        function next(arg) {
            var tempSortedKeys = Object.keys(elems).sort();
            var next = ((tempSortedKeys.indexOf(arg) === tempSortedKeys.length - 1)) ? 0 : (tempSortedKeys.indexOf(arg) + 1);
            var nextKey = tempSortedKeys[next];
            var nextRet = {};
            nextRet[nextKey] = elems[nextKey];
            return nextRet;
        }
        function prev(arg) {
            var tempSortedKeys = Object.keys(elems).sort();
            var prev = (tempSortedKeys.indexOf(arg) === 0) ? (tempSortedKeys.length - 1) : (tempSortedKeys.indexOf(arg) - 1);
            var prevKey = tempSortedKeys[prev];
            var prevRet = {};
            prevRet[prevKey] = elems[prevKey];
            return prevRet;
        }
        function value(arg) {
            if(elems[arg]){
		return elems[arg];
            }
            else{
                return 'Nie ma takiego klucza w słowniku.';
            }
	}
        function show() {
            return elems;
	}
        function removeByValue(arg){
            for(var i in elems){
                if(elems[i]==arg){
                    delete elems[i];
                }
            }
        }
        return {
            hasValue : hasValue,
            hasKey : hasKey,
            add : add,
            remove : remove,
            update : update,
            next: next,
            prev : prev,
            value : value,
            show : show,
            removeByValue : removeByValue
	}
};
var myDictionary = new Dictionary();



//1. Napisz skrypt wykorzystujący obiekt, którego zadaniem będzie przetłumaczenia tekstu na język angielski. Zdefiniuj słownik słów i wykaż przetłumaczony tekst na przykładzie. Jeżeli brakuje słowa w słowniku, pozostaw puste.

var PolishEnglish = function(){
    this.dict=new Dictionary();
    var tabTwo=[];
    this.translate=function(arg){
    	var tab=arg.split(' ');
	    for(var i=0; i<tab.length; i++){
	    	if(this.dict.hasKey(tab[i])){
	            tabTwo.push(this.dict.value(tab[i]));
	        }
                else{
                    tabTwo.push(null);
                }
	    }
	    return tabTwo.join(' ');
    }
};
var myPolishEnglish = new PolishEnglish();
var dictone = {ja:'I',ty:'you',on:'he',ona:'she',biegać:'run',chodzić:'walk',mój:'my', jego:'his',jej:'her',pies:'dog', kot:'cat', niebieski:'blue', zielony:'green', żółty:'yellow', jestem:'am',jest:'is',jesteś:'are'};

myPolishEnglish.dict.add(dictone);
var x='mój pies jest zielony';
myPolishEnglish.translate(x);

//
//2. Stwórz obiekt, który wykorzystując strukturę słownika zapewni tłumaczenie słów jak w encyklopedii. Program powinien działać, dopóki użykownik nie wpisze '-'. Przykład:
//Wejście: 'Koń' 
//Wyjście: 'Koń jaki jest każdy widzi'

var Encyclopaedia = function(){
    this.dict=new Dictionary();
    this.description = function(){
        var x;
        do{
            x = prompt('Podaj słowo lub zakończ działanie myślnikiem');
            if(x=='-') break;
            if(this.dict.hasKey(x)){
                alert(this.dict.value(x));
            }
            else {
                alert ('Nie ma takiego hasła w encyklopedii');
            }
        }
        while (x!=='-')
    };
};
var myEncyclopaedia = new Encyclopaedia();
var dictTwo={koń:'Koń jaki jest każdy widzi', słoń:'Trochę jak koń, ale większy', świnka:'przeszłość szynki', człowiek:'spójrz w lustro', komar:'mini wampir'};
myEncyclopaedia.dict.add(dictTwo);
myEncyclopaedia.description();