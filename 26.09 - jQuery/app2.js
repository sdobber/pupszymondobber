$(document).ready(function(){
   var $wrap = $('.wrapper');
   var $loginBox = $('.loginBox');
   var pTxt = $wrap.children('p').html();
   var $secPara = $('.secPara');
   var $zaloguj = $('#send');
   var $login = $('#login');
   var $pass = $('#password');
   var $usuntytul = $('.firstBtn');
   var $pokazinfo = $('.secondBtn');
   var $wrap5 = $('.wrap5');
   
   pTxt = pTxt.split('.')[0]+'.';
   var n=0;
   $wrap.children('h2').html(pTxt).addClass('redTxt blueBorder');
   
   //ukrywamy button
   $loginBox.hide();
   $('#logMeIn').on('click', function(){
      if ($loginBox.is(':visible')) {
          $loginBox.hide();
      }
      else {
          $loginBox.show();
      }
      n++;
      if (n==6){
          $(this).off('click'); //bo odnosimy sie do argumentu przekazanego do function, czyli #logMeIn
          $wrap.append('<p>Kliknąłeś już '+n+' razy!</p>').children('p:last').addClass('blueTxt');
      }
      
   });
   $secPara.find('span').css({'color':'red', 'background-color':'yellow'});
   //po wcisnieciu przycisku zamien, zostala pobrana wartosc z tego inputa, a nastepnie zamieniony tekst na pobrany z inputa we wszystkich spanach w tym akapicie
   $('.changeBtn').on('click', function (){
       $secPara.find('span').text($secPara.children('input').val());
   });
   console.log('Pola name ma wartość: ' +$secPara.find('input').attr('name'));
   $secPara.find('input').attr('name', 'nowyName');
   console.log('Pole name ma wartość: '+$secPara.find('input').attr('name'));
   
   var users = [
       {name: 'John', pass:'qwerty'},
       {name: 'Jane Doe', pass:'unknown'}
   ];
   $secPara.children('input').on('keyup', function(){
       if ($(this).val().length>3){
          // console.log($(this).val());
       }
   });
   
   // w loginboxie po nacisnieciu zaloguj, sprawdzamy czy istnieje uzytkownik o takich parametrach, jezeli tak, na zielono w paragrafie w tym divie wpisujemy 'Dane sa poprawne' , jezeli nie, wpisujemy na czerwono 'podano zle dane'
   $zaloguj.on('click', function(){
      for (var i=0; i<users.length; i++){
      if ($login.val() == users[i].name && $pass.val() == users[i].pass) {
          
          $loginBox.children('p').html('Dane są poprawne.').css('color', 'green');
          $loginBox.removeClass('w3-animate-right');
          $loginBox.addClass('w3-animate-left-right');
          setTimeout(function(){ $wrap.find('div').remove('.loginBox'); }, 1499);
          return false;
      }
      else {
          $loginBox.children('p').append('Podano złe dane.').css('color', 'red');
          return false;
      }
  };
   });
   // 1 napiszmy kod, ktory po wcisnieciu przycisku 'usun tytul' usunie diva o klasie 'title' z diva o klasie 'wrap5'
   //2 napiszmy kod, ktory po wcisnieciu przycisku 'pokaz informacje' wyswietli nam tekst zawarty w datasecie, a konkretnie w polu msg.
   $usuntytul.on('click', function(){
       $wrap5.find('div').remove('.title');
   });
  $pokazinfo.on('click', function(){
      console.log($wrap5.find('.info').data('msg'));
      $wrap5.find('.info').data('msg','Udalo sie');
      console.log($wrap5.find('.info').data('msg'));
  });
  
    //dla parzystych elementow listy ustawmy kolor tla na zolty, a kolor tekstu na czerwony, dla nieparzystych elementow listy kolor tla na czarny, a kolor tekstu na bialy
  var $myElems = $('ul').children('li');
  $myElems.each(function (index, elem){ //tak jak for each
      // elem == this
      if(index%2==0){
          $(elem).css({'background-color':'black', 'color':'white'});
      }
      else
      {
          $(elem).css({'background-color':'yellow', 'color':'red'});
      }
      
  });
  // dla elementow z ol'a, ktore nie maja przypisanej klasy .nono, dodaj klase .redTxt w przeciwnym wypadku dodaj klase .blueBorder
  
  var $myElems2 = $('ol').children('li');
  $myElems2.each(function(){
     if($(this).hasClass('nono')){
         $(this).addClass('redTxt');
     } 
     else
     {
         $(this).addClass('blueBorder')
     }
  });

});