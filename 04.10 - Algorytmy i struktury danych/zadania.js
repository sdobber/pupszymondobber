//1 napisz funkcje ktora zliczy ilosc znakow w ciagu znakow (string). wynik funkcji powinien byc taki sam
//jak uzycie length. zakladamy, ze ciag znakow jest predefiniowany

var myString = "hello world";

function countChars (arg){
	for(var i in arg);
	return ++i;
}

countChars(myString);


//napisz funkcje ktora dla zadanego ciagu znakow zliczy wystepowanie poszczegolnych znakow. przykladowo dla slowa  'programowanie' powinien zostac zwrocony slownik (tablica asocjacyjna) z przypisana iloscia znakow:
//{'p':1, 'r':2, 'o':2 ...} (niekoniecznie w tej kolejnosci)

var myString = 'programowanie';

function countLetters (arg){
	var dictionary ={}; //definicja pustego slownika
	for(var i=0; i<myString.length; i++){ //iteracja przez wszystkie elementy argumentu(myString)
		if(dictionary[arg[i]]){
			dictionary[arg[i]]++;
		}
		else {
			dictionary[arg[i]]=1;	
		}
		
	}
	return dictionary;
	
}

countLetters(myString);



//Napisz funkcję, która zwróci dla zadanego ciągu znaków nowy ciąg składający się z pierwszych trzech oraz ostatnich trzech znaków ciągu. W przypadku, gdy ciąg ma mniej niż 3 znaki - zwróć pustą wartość. Nie używaj funkcji substring.

var myString = 'fajneslowo';

function partOfWord (arg){
	if (arg.length<3){
		return null;
	}
	else
	{
		var tab =[];
		for(var i=0; i<3; i++){
			tab.push(arg[i]);
		}
		for (var i=arg.length-3; i<arg.length; i++){ //mozna zrobic w jednym forze by do tablicy pushowac wszystko poza zakresem od 3 do length-3
			tab.push(arg[i]);
		}
	}
	return tab.join('');
}

partOfWord(myString);

/* jeszcze inna wersja
 var start='', end='';
for(var i=0; i<3; i++){
start+=arg[i];
end+=arg[arg.length-3+i];
}
return start+end;
 */

//Napisz funkcję, która dla dwóch zadanych ciągów znaków, zwróci jeden, rozdzielony spacją z zamienionymi pierwszymi znakami każdego łańcucha. Przykład: 
//Wejście: 'javascript', 'zadanie' (jako arg. f-cji) 
//Wyjście: 'zavascript jadanie'.
//W tym zadaniu nie używajmy tablic.



function replacer (){
	return arguments[1].substring(0,1)+arguments[0].substring(1)+' '+arguments[0].substring(0,1)+arguments[1].substring(1);
}

replacer('javascript', 'zadanie')

//Angielski. Napisz funkcję, która dla zadanego łańcucha znaków doda końcówkę 'ing'. Jeżeli ciąg znaków już kończy się na 'ing' - zamiast tego należy dodać 'ly'. Jeżeli łańców znaków ma długość krótszą niż 3 znaki - pozostaw ciąg bez zmian
//W tym zadaniu nie używajmy tablic.

function changeWord (){
	if (arguments[0].length<3){
		return arguments[0];
	}
	else
	{
		if(arguments[0].substring(arguments[0].length-3,arguments[0].length)=='ing'){
			return arguments[0]+'ly';
		}
		else {
			return arguments[0]+'ing';
		}
	}
}
changeWord('pi');

//wersja jednolinijkowa
//return (arguments[0].length<3) ? arguments[0] : (arguments[0].substring(arguments[0].length-3,arguments[0].length)=='ing') ? arguments[0]+'ly' : arguments[0]+'ing';


//Napisz funkcję, która znajdzie pierwsze wystąpienie słowa 'nie' oraz 'zly' w zadanym ciągu znaków. Jeżeli słowo 'nie' występuje przed słowem 'zly' (pomiędzy nimi mogą znajdować się inne słowa), zastąp całość słowem 'nienajgorszy'. Dla przykładu: 
//Wejście: Tekst jest niezly! 
//Wyjście: Tekst jest nienajgorszy! 

//Wejście: Tekst jest nie taki zly! 
//Wyjście: Tekst jest nienajgorszy!

function replaceWords (){
	if(arguments[0].indexOf('nie')<arguments[0].indexOf('zly')){
		return arguments[0].substring(0,arguments[0].indexOf('nie'))+'nienajgorszy';
	}
	else {
		return arguments[0].substring(0,arguments[0].indexOf('nie'))+'nienajgorszy'+arguments[0].substring(arguments[0].indexOf('nie'));
	}
}
console.log(replaceWords('Tekst jest niezly'));
console.log(replaceWords('Tekst jest nie taki zly'));
console.log(replaceWords('Tekst zly jest nie taki zly hehe vbcbv'));


//wersja z zajęć
//var str = 'Tekst zly jest nie taki zly zly hehe',
//    strTwo = 'Tekst jest niezly i tyle.';
//    
//    
//function changeWords(arg) {
//    var a = arg.indexOf('nie'),
//        b = arg.substring(a).indexOf('zly') + a + 'zly'.length;
//    return arg.substring(0, a) + 'nienajgorszy' + arg.substring(b);
//}
//
//console.log(changeWords(str));
//console.log(changeWords(strTwo));




//Napisz funkcję, która dla zadanego ciągu znaków oraz numerze indeksu, zwróci ten ciąg bez znaku, który znajduje się na zadanym miejscu. Wydrukuj informację, jeżeli zadany indeks jest większy niż długość ciągu. Przykład:

//Wejście (argumenty): 'javascript', 0 
//Wyjście: 'avascript' 

//Wejście: 'javascript', 5 
//Wyjście: 'javasript'

//Wskazówka, żeby było łatwiej: Nie używaj tablic oraz funkcji substring(), slice()

function withoutIndex (arg, argTwo){
	var word ='';
	for (var i in arg){
		if(i==argTwo){
			continue;
		}
		word+=arg[i]
	}
	return word;
}

withoutIndex('javascript',5);



//Napisz funkcję, która zamienia dla zadanego ciągu znaków pierwszy znak z ostatnim. Dla ułatwienia nie używaj funkcji substring, slice oraz tablic.


function replaceChars (){
	var temp =arguments[0][arguments[0].length-1];
	var word = '';
	for (var i in arguments[0]){
		if(i==0){
			word=temp;
		}
		else{
                        word+=arguments[0][i];
		}
		if(i==8){
			word+=arguments[0][0];
			return word;
		}
	}

}
replaceChars('javascript');


//wersja z zajęć
var str = 'javascript';

function swapLetters(arg) {
    var temp = arg[arg.length-1];
    for(var i = 1; i < arg.length-1; i++) temp += arg[i];
    return temp + arg[0];
}

swapLetters(str);


//Napisz funkcję, która usunie z zadanego ciągu znaków znaki o nieparzystych indeksach.

function removeOdd (){
	var word='';
	for (var i in arguments[0]){
		if(i%2==0){
                    word+=arguments[0][i];
		}
	}
	return word;
}
removeOdd('javascript');


//Napisz funkcję, która zliczy ile razy występuje każde ze słów POD NIEPARZYSTYM INDEKSEM w przekazanym zdaniu jako argument.

//Dla ułatwienia użyj tablicy asocjacyjnej.


function countWords(){
	var dictionary={};
	var tab=arguments[0].split(' ');
	for (var i in tab){
		if(i%2==1){
			if (dictionary[tab[i]]){
				dictionary[tab[i]]++;
			}
			else{
				dictionary[tab[i]]=1;
			}
		}
	}
	return dictionary;
}

countWords('nie wiem co tam nie wiem co tu i tyle');


//Napisz funkcję, która w zadanej liście zliczy ilość słów, które zaczynają i kończą się tą samą literą. Pomiń obliczenia dla słów, które mają mniej niż 3 znaki.

//Dla ułatwienia koniecznie użyj funkcji charAt()

var wordList = ['lalal','costam', 'nic', 'kajak', 'eliscie', 'apteka', 'banan'];
var counter=0;
function beginEnd (arg){
	for (var i=0; i<arg.length; i++){
		if (wordList[i].length>=3){
			if(wordList[i].charAt(0)==wordList[i].charAt(wordList[i].length-1)){
				counter++;
			}
		}
	}
	return counter;
}
beginEnd(wordList);

//Napisz funkcję, która usunie duplikaty liczb znajdujących się na liście. Przykład: 
//Wejście: [1, 2, 3, 2, 3, 2, 4] 
//Wyjście: [1, 2, 3, 4]

var tab = [1,2,3,2,3,2,4];

function deleteDuplicates (arg) {
	var tabTwo =[];
	for(var i=0; i<tab.length; i++){
		if (tabTwo.indexOf(tab[i])==-1){
			tabTwo.push(tab[i]);
		}
	}
	return tabTwo;
}

deleteDuplicates(tab);

//napisz funkcję (bez argumentow) ktora zwroci slownik wypelniony parami liczb:
//{liczba : kwadrat_liczb, ...}
//dla liczb od 1 do 10

function square (){
	var dictionary={};
	for (var i=1; i<11; i++){
            dictionary[i]=i*i;
	}
	return dictionary;
}
square();



//napisz funckje, ktora obliczy sume elementow w tablicy zagniezdzonej
function sumArrElems (arr){
	var sum = 0;
	if(typeof arr == 'number'){
		return sum+=arr;
	}
	for (var i in arr){
		if (typeof arr[i] =='number'){
			sum = sum + arr[i];
		}
		else if (typeof arr[i] == 'object'){
			sum = sum + sumArrElems(arr[i]);
		}
	}
	return sum;
}

sumArrElems([[1, 2],[[3,4],[5,6]], [7],8]);

//Napisz funkcję, która przyjmuje dwa argumenty dwa słowniki, które mają przypisane wartości w stylu { 'string' : number, ... } i zwraca jeden, który jest połączeniem tych dwóch argumentów. Jeżeli w obu słownikach występuje ten sam klucz, to wartość numeryczna powinna być sumą wartościa danego klucza dla dwóch słowników.

//Przykład:

//Weście: var x = { 'a' : 1, 'b' : 2 }
 //          y = { 'a' : 3, 'c' : 4}
           
//Wyjście: { 'a' : 4, 'b' : 2, 'c' : 4 } /* kolejność argumentów jest dowolna */
var x = {'a':1, 'b':2};
var y = {'a':3, 'c':4};

function sumDict (arg, argTwo){
	
	
	for (var i in arg){		//iterujemy po pierwszym slowniku
	
		if (argTwo[i]){		//sprawdzamy czy istnieje, a ze iterujemy po pierwszym
				argTwo[i]=arg[i]+argTwo[i];
			}
			else{
				argTwo[i]=arg[i];
			}
	}
	return argTwo;
}

sumDict(x,y);

//Napisz funkcję, która dla wczytanego od użytkownika słowa, zwróci string zawierający najpierw jego litery pod parzystymi indeksami, następnie pod nieparzystymi, ale te drugie w kolejności odwrotnej. Koniecznie użyj funkcji .charAt()
var str = prompt("podaj słowo");
function changeWord (arg){
	var tab = [];
	var tabTwo = [];
	for (var i=0; i<arg.length; i++){
		if (i%2==0){
			tab.push(arg.charAt(i));
		}
		else {
			tabTwo.push(arg[i]);
		}
	
		
	}
	tabTwo.reverse();
	return tab.join('')+tabTwo.join('');
}

changeWord(str);


//Funkcja przyjmuje dwa słowniki (jako argumenty). Korzystając z metody charCodeAt() zbadajmy, czy wartość pod danym kluczem jest parzysta czy nie. Jeżeli pierwsza litera w kluczu ma wartość parzystą i wartość klucza jest także liczbą parzystą, dodajmy ten klucz do tablicy ( [] ), jeżeli obie wartości są nieparzyste także dodajemy klucz do tablicy. W przypadku gdy wartość klucza jest nieparzysta, a pierwsza litera klucza ma wartość parzystą taki klucz pomijamy - podobnie w sytuacji odwrotnej. Podsumowując, w tablicy powinny się znaleźć tylko klucze, które spełniają dane zależności:
  //  > parzystość pierwszej litery klucza i wartości klucza musi być taka sama
   
//Podpowiedź: console.log('a'.charCodeAt(0));    

//Wejście: var x = { 'pierwszy' : 'a', 'drugi' : 'b', 'trzeci' : 'c' },
  //           y = { 'czwarty'  : 'd', 'piaty' : 'e', 'szosty' : 'f' };

var x = { 'pierwszy' : 'a', 'drugi' : 'b', 'trzeci' : 'c' },
    y = { 'czwarty'  : 'd', 'piaty' : 'e', 'szosty' : 'f' };
function keyValue (arg, argTwo){
	var tab=[];
	for (var i in arg){		//iterujemy po pierwszym slowniku
            argTwo[i]=arg[i]; //nie musimy sprawdzac czy istnieje, bo slowniki sa calkowicie inne
	}
	for (var i in argTwo){
	if(i.charCodeAt(0)%2=== argTwo[i].charCodeAt(0)%2){ //nie trzeba przyrownywac do zera ani do jeden tylko mozna zwyczajnie do siebie
                                                            //i dziala zarowno dla parzystych i nie parzystych
		tab.push(i);
	}
	}
	return tab;
}
keyValue(x,y);


// używanie slownika
/*
 var dict = {'asd':5, 'qwe':7};
var z = 'key';
dict[z]=11;
dict['innyKlucz'] = 19;
dict.jeszczeInnyKlucz = 21;
for(var key in dict) console.log(key,dict[key]);
 */


//Mamy zdefiniowany słownik, który jest przekazany dla funkcji jako argument. Zwróćmy słownik, który będzie miał przypisane wartości jako klucze, a klucze jako wartości.


var x={'pierwszy':1, 'drugi':2, 'trzeci':3};

function changeKeysAndValues (arg){
	
	var dict={};
	for (var i in arg){
		dict[arg[i]]=i; //tak samo jak sorotowanie babelkowe
		
	}
	return dict;
}
changeKeysAndValues(x);

//Mamy zdefiniowany i przekazany jako argument słownik. Funkcja powinna zwrócić tablicę, która będzie zawierała w sobie inne tablice z kluczami i wartościami np.

//Wejście: { 'klucz' : 'wartosc', 'innyKlucz' : 'innaWartosc' }
//Wyjście: [ ['klucz', 'wartosc'], ['innyKlucz', 'innaWartosc'] ];


var x = { 'klucz' : 'wartosc', 'innyKlucz' : 'innaWartosc' };
function addToTab (arg){
	var tab=[];
	for (var i in arg){
		
		tab.push([i,arg[i]]);
	}
	return tab;
}
addToTab(x);



//Do funkcji przekazujemy słownik, który posiada klucze i przypisane do nich wartości numeryczne. Funkcja powinna zwracać drugi kwartyl (medianę).

//Wejście: { 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20 }
//Wyjście: 7

//Wejście: { 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20, 'i' : 13 }
//Wyjście: 8

//nie uzywamy funkcji sort();

var x ={ 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20 };
var y ={ 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20, 'i' : 13 };
function mediane (arg){
	var tab=[];
	for(var i in arg){
		tab.push(arg[i]);
	}
	tab=sortTab(tab);
	if (tab.length%2==1){
		return tab[Math.floor(tab.length/2)];
	}
	else{
		return Math.floor(tab[Math.floor(tab.length/2)]+tab[(Math.floor(tab.length/2))-1])/2;
	}
}
function sortTab (arg){
	var temp;
        n=arg.length-1;
	do{
            for (var j=0; j<arg.length-1; j++){
                    if(arg[j]>arg[j+1]){
                            temp=arg[j];
                            arg[j]=arg[j+1];
                            arg[j+1]=temp;
                    }
            }
            n=n-1;
        }while (n>1)
	return arg;
}
mediane(x);
mediane(y);

//liczenie sumy elementow macierzy
var arr = [[1, 2, 9],[3, 4, 9],[5, 6, 9],[7, 8, 9]];
var sum=0;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			sum=sum+arr[i][j];
		}
	}
	return sum;
}
arraySum(arr);

//1. Napisz program sumujący elementy podzielne przez 7 -tablicy dwuwymiarowej
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
var sum=0;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]%7==0){
				sum=sum+arr[i][j];
			}
		}
	}
	return sum;
}
arraySum(arr);


//2. Napisz program obliczający iloczyn elementów tablicy dwuwymiarowej

var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
var sum=1;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			sum=sum*arr[i][j];
		}
	}
	return sum;
}
arraySum(arr);

//3. Napisz program obliczający iloczyn elementów parzystych-tablicy dwuwymiarowej 
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
var sum=1;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]%2==0){
				sum=sum*arr[i][j];
			}
		}
	}
	return sum;
}
arraySum(arr);


//4. Napisz program obliczający iloczyn elementów nieparzystych - tablicy dwu wymiarowej
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
var sum=1;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]%2==1){
				sum=sum*arr[i][j];
			}
		}
	}
	return sum;
}
arraySum(arr);

//5. Napisz program obliczający iloczyn elementów podzielnych przez 7 -tablicy dwuwymiarowej 
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
var sum=1;
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]%7==0){
				sum=sum*arr[i][j];
			}
		}
	}
	return sum;
}
arraySum(arr);

//6. Napisz program znajdujący minimalny element-tablicy dwuwymiarowej 
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
function arraySum (arg){
	var min=arr[0][0];
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]<min){
				min=arr[i][j];
			}
		}
	}
	return min;
}
arraySum(arr);

//7. Napisz program znajdujący maksymalny element-tablicy dwuwymiarowej 
var arr = [[1, 7, 9],[21, 4, 9],[35, 6, 9],[7, 8, 13]];
function arraySum (arg){
	var max=arr[0][0];
	for(var i=0; i<arr.length; i++){
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]>max){
				max=arr[i][j];
			}
		}
	}
	return max;
}
arraySum(arr);

//8. Napisz program sumujący elementy parzyste w każdym wierszu-tablicy dwuwymiarowej 


var arr = [[1, 2, 9],[6, 4, 9],[5, 6, 9],[7, 8, 9]];
var sum;
var tab=[];
function arraySum (arg){
	for(var i=0; i<arr.length; i++){
		sum=0;
		for (var j=0; j<arr.length-1; j++){
			if(arr[i][j]%2==0){
			sum=sum+arr[i][j];
			}
		}
		tab.push(sum);
	}
	return tab;
}
arraySum(arr);

//9. Napisz program, sprawdzający czy wczytana liczba z klawiatury jest liczbą pierwszą.

var tablica=[];
function isPrime(n) {
   for (var i = 2; i < n; i++) {
       if (n % i == 0) {
           return false;
       }
   }
   return true;
}
function printPrimesFromRange(a, b) {
   for (var i = a; i <= b; i++) {
       if (isPrime(i)) {
           tablica.push(i);
       }
   }
}
printPrimesFromRange(2, 10000);

function isItAPrime(tab){
	var suma=0;
	var liczba=parseInt(tab);
	var counter=0;
	for (var i=0; i<tablica.length; i++){
		if(liczba==tablica[i]){
			counter++;
		}
	}
	if(counter==0){
		return 'Liczba '+liczba+' nie jest liczbą pierwszą';
	}
	else{
		return 'Liczba '+liczba+' jest liczbą pierwszą';
	}
	
}
var x = prompt('Podaj liczbę.');

isItAPrime(x);

//Napisz obiekt, a w nim funkcje które dla wartości wczytanej z klawiatury zliczą ilość występujących
//liter oraz cyfr we wczytanym ciągu. Obiekt powinien posiadać funkcję sprawdzającą czy dany znak
//jest cyfrą, literą oraz funkcję dokonującą zliczeń. Obiekt powinien posiadać argument przy tworzeniu
//instancji, który będzie stringiem.

function countCharacters(arg) {
    
    this.arg = arg;
    var tab = this.arg.split('');
    var tabTwo=[];
    var letterCounter=0;
    var numberCounter=0;
    for(var i=0; i<tab.length; i++){
            if(tab[i].charCodeAt(0)!=32){
                tabTwo.push(tab[i]);
            }
        }
    function countLetters(arg){
        if(isNaN(parseInt(arg))){
            letterCounter++;
        }
    }
    function countNumbers(arg){
        if(!isNaN(parseInt(arg))){ //nie mozemy sprawdzac w sposob cos===NaN
            numberCounter++;
        }
    }
    this.count = function(arg){
        for(var i=0; i<tabTwo.length;i++){
            countLetters(tabTwo[i]);
            countNumbers(tabTwo[i]);
        }
        return 'Liter jest '+letterCounter + ' a cyfr jest '+numberCounter;
    }
}
var x=prompt('Podaj ciąg znaków - liter i cyfr');

var myObject = new countCharacters(x);
console.log(myObject.count());


//Napisz obiekt, który będzie posiadał min. dwie metody, wyswietl() oraz wczytaj(). Metoda wczytaj()
//powinna zapytać użytkownika o ciąg znaków, metoda wyświetl() powinna przyjąć co najmniej jeden
//argument, który w zależności od wyboru ('duze', 'male') wyświetli pobrany ciąg znaków dużymi lub
//małymi literami.
function smallBig (){
    var x='';
	this.wczytaj = function(){
        x=prompt('Podaj ciąg znaków');
    };
    this.wyswietl = function(argTwo){
        if(argTwo=='male'){
			return x.toLowerCase();
		}
		if(argTwo=='duze'){
			return x.toUpperCase();
		}
    };
    
}
var myObject = new smallBig();
myObject.wczytaj();
console.log(myObject.wyswietl('duze'));
console.log(myObject.wyswietl('male'));
